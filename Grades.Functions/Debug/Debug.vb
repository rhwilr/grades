﻿Imports Grades.DB.Repository
Imports Grades.DB.Model

Public Class Debug
    Public Shared Sub Debug()

        CreateDataRecords()
        DebugSettings()
    End Sub

    Private Shared Sub CreateDataRecords()

        Connection.GradesContext.ExecuteStoreCommand("DELETE Grade")
        Connection.GradesContext.ExecuteStoreCommand("DELETE Subject")
        Connection.GradesContext.ExecuteStoreCommand("DELETE Schoolday")
        Connection.GradesContext.ExecuteStoreCommand("DELETE Term")
        Connection.GradesContext.ExecuteStoreCommand("DELETE School")


        Dim school_bm As New School
        Dim school_bs As New School

        Dim subject_deu As New Subject
        Dim subject_eng As New Subject
        Dim subject_mat As New Subject
        Dim subject_inf As New Subject
        Dim subject_nwg As New Subject

        Dim schoolday_bm As New Schoolday
        Dim schoolday_bs As New Schoolday

        Dim term_1 As New Term
        Dim term_2 As New Term
        Dim term_3 As New Term

        Dim grade_1 As New Grade
        Dim grade_2 As New Grade
        Dim grade_3 As New Grade
        Dim grade_4 As New Grade
        Dim grade_5 As New Grade
        Dim grade_6 As New Grade
        Dim grade_7 As New Grade
        Dim grade_8 As New Grade

        school_bm.Identifier = "Berufsmatura"
        school_bs.Identifier = "Berufsschule"

        Connection.GradesContext.School.AddObject(school_bm)
        Connection.GradesContext.School.AddObject(school_bs)


        subject_deu.Identifier = "Deutsch"
        subject_deu.School = school_bm

        subject_eng.Identifier = "English"
        subject_eng.School = school_bm

        subject_mat.Identifier = "Mathematik"
        subject_mat.School = school_bm

        subject_inf.Identifier = "Informatik"
        subject_inf.School = school_bs

        subject_nwg.Identifier = "NWGL"
        subject_nwg.School = school_bs

        Connection.GradesContext.Subject.AddObject(subject_deu)
        Connection.GradesContext.Subject.AddObject(subject_eng)
        Connection.GradesContext.Subject.AddObject(subject_mat)
        Connection.GradesContext.Subject.AddObject(subject_inf)
        Connection.GradesContext.Subject.AddObject(subject_nwg)


        schoolday_bm.Schoolday1 = 4
        schoolday_bm.School = school_bm

        schoolday_bs.Schoolday1 = 0
        schoolday_bs.School = school_bs

        Connection.GradesContext.Schoolday.AddObject(schoolday_bm)
        Connection.GradesContext.Schoolday.AddObject(schoolday_bs)


        term_1.Identifier = "Semester 1"
        term_1.Date_Start = New Date(2013, 1, 1)
        term_1.Date_End = New Date(2013, 7, 31)
        term_1.School = school_bm

        term_2.Identifier = "Semester 2"
        term_2.Date_Start = New Date(2013, 8, 1)
        term_2.Date_End = New Date(2013, 12, 31)
        term_2.School = school_bm

        term_3.Identifier = "Semester 1"
        term_3.Date_Start = New Date(2013, 1, 1)
        term_3.Date_End = New Date(2013, 7, 31)
        term_3.School = school_bs

        Connection.GradesContext.Term.AddObject(term_1)
        Connection.GradesContext.Term.AddObject(term_2)
        Connection.GradesContext.Term.AddObject(term_3)


        grade_1.Identifier = "Winkel im Dreieck"
        grade_1.Subject = subject_mat
        grade_1.Term = term_1
        grade_1.Weight = 1
        grade_1.Grade1 = 5.3141592
        grade_1.Comment = "Wiederholungsprüffung"
        grade_1.Date = New Date(2013, 6, 11)

        grade_2.Identifier = "Rede über Kapitalismus"
        grade_2.Subject = subject_deu
        grade_2.Term = term_1
        grade_2.Weight = 1
        grade_2.Grade1 = 4.8
        grade_2.Date = New Date(2013, 5, 19)

        grade_3.Identifier = "Future tense"
        grade_3.Subject = subject_eng
        grade_3.Term = term_1
        '   grade_3.Weight = 1
        '   grade_3.Grade1 = 5.9
        grade_3.Comment = "Klassenschnitt 3.2"
        grade_3.Date = New Date(2013, 2, 26)

        grade_4.Identifier = "MKN Modul 132"
        grade_4.Subject = subject_inf
        grade_4.Term = term_3
        grade_4.Weight = 1
        grade_4.Grade1 = 5.5
        grade_4.Comment = ""
        grade_4.Date = New Date(2013, 6, 10)


        grade_5.Identifier = "MKN Modul 117"
        grade_5.Subject = subject_inf
        grade_5.Term = term_3
        grade_5.Weight = 1
        grade_5.Grade1 = 3.5
        grade_5.Comment = "War ein schlechter Tag, hat geregnet"

        grade_6.Identifier = "Quadratische Funktionen"
        grade_6.Subject = subject_mat
        grade_6.Term = term_1
        grade_6.Weight = 2
        grade_6.Grade1 = 4.5
        grade_6.Comment = ""

        grade_7.Identifier = "Phytagoras"
        grade_7.Subject = subject_mat
        grade_7.Term = term_1
        grade_7.Weight = 1
        grade_7.Grade1 = 5.0
        grade_7.Comment = ""

        grade_8.Identifier = "Nomen im Text erkennen"
        grade_8.Subject = subject_deu
        grade_8.Term = term_2
        grade_8.Weight = 1
        grade_8.Grade1 = 5.9
        grade_8.Date = New Date(2012, 11, 19)

        Connection.GradesContext.Grade.AddObject(grade_1)
        Connection.GradesContext.Grade.AddObject(grade_2)
        Connection.GradesContext.Grade.AddObject(grade_3)
        Connection.GradesContext.Grade.AddObject(grade_4)
        Connection.GradesContext.Grade.AddObject(grade_5)
        Connection.GradesContext.Grade.AddObject(grade_6)
        Connection.GradesContext.Grade.AddObject(grade_7)
        Connection.GradesContext.Grade.AddObject(grade_8)

        Connection.GradesContext.SaveChanges()
    End Sub


    Private Shared Sub DebugSettings()
    End Sub
End Class

﻿Imports System.Windows.Forms

Public Class GetActive
    Public Shared Function School(ByVal ctrl As Ribbon) As Integer

        Dim _recCtrl = (From recCtrl In ctrl.OrbDropDown.RecentItems Where recCtrl.Checked = True).FirstOrDefault()

        If Not IsNothing(_recCtrl) Then

            Return _recCtrl.Value
        Else
            Return Nothing
        End If

    End Function

    Public Shared Function Subject(ByVal ctrl As Ribbon) As Integer

        Dim _tabCtrl = (From tabCtrl In ctrl.Tabs Where tabCtrl.Active = True).FirstOrDefault()

        If Not IsNothing(_tabCtrl) Then

            Return _tabCtrl.Value
        Else
            Return Nothing
        End If

    End Function

    Public Shared Function Term(ByVal ctrl As Ribbon) As Integer
        Return _
            Controls.FindRibbonComboBox(
                "rc_term_term_:" & (From tabCtrl In ctrl.Tabs Where tabCtrl.Active = True).FirstOrDefault().Value, ctrl) _
                .FirstOrDefault().SelectedItem.Value
    End Function
End Class

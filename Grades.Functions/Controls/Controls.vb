﻿Imports System.Windows.Forms

Partial Public Class Controls
    Public Shared Function FindRibbonButton(ByVal Value As String, ByVal ctrl As Ribbon) As List(Of RibbonButton)

        Dim RibbonItemList As New List(Of RibbonButton)

        For Each panCtrl In From tabCtrl In ctrl.Tabs From panCtrl1 In tabCtrl.Panels Select panCtrl1
            Dim panContrl = panCtrl
            RibbonItemList.AddRange(
                (From itmCtrl In panContrl.Items Where itmCtrl.Tag Like Value).Cast (Of RibbonButton)())
        Next

        Return RibbonItemList
    End Function

    Public Shared Function FindRibbonComboBox(ByVal Value As String, ByVal ctrl As Ribbon) As List(Of RibbonComboBox)

        Dim RibbonItemList As New List(Of RibbonComboBox)

        For Each panCtrl In From tabCtrl In ctrl.Tabs From panCtrl1 In tabCtrl.Panels Select panCtrl1
            Dim panContrl = panCtrl
            RibbonItemList.AddRange(
                (From itmCtrl In panContrl.Items Where itmCtrl.Tag Like Value).Cast (Of RibbonComboBox)())
        Next

        Return RibbonItemList
    End Function


    Public Shared Function FindRibbonOrbRecentItem(ByVal Value As String, ByVal ctrl As Ribbon) _
        As List(Of RibbonOrbRecentItem)

        Return _
            (From recCtrl In ctrl.OrbDropDown.RecentItems Where recCtrl.Tag Like Value).Cast (Of RibbonOrbRecentItem)().
                ToList()
    End Function
End Class

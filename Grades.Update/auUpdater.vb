﻿Imports wyDay.Controls

Public Class auUpdater
    Const GUID = "ac847ee4-add3-4102-ae7a-8b2b3d6a6f78"
    Const wyUpdateLocation = "Update.exe"
    Const CheckDays = 5


    Public Event UpdateSuccessful()
    Public Event UpdateAvailable()
    Public Event UpdateUpToDate()
    Public Event ReadyToBeInstalled()
    Public Event ProgressChanged()

    Public ReadOnly Property ClosingForInstall As Boolean
        Get
            Return _ClosingForInstall
        End Get
    End Property

    Public ReadOnly Property UpdateReadyToBeInstalled As Boolean
        Get
            Return _UpadteReadyToBeInstalled
        End Get
    End Property

    Public ReadOnly Property UpdateAvailableToBeInstalled As Boolean
        Get
            Return _UpdateAvailableToBeInstalled
        End Get
    End Property

    Public ReadOnly Property Version() As String
        Get
            Return auBackend.Version
        End Get
    End Property

    Public ReadOnly Property UpdateStepOnString() As String
        Get
            Return auBackend.UpdateStepOn.ToString()
        End Get
    End Property


    Private _ClosingForInstall As Boolean
    Private _UpadteReadyToBeInstalled As Boolean = False
    Private _UpdateAvailableToBeInstalled As Boolean = False

    Private WithEvents auBackend As AutomaticUpdaterBackend

    Public Sub New()

        auBackend = New AutomaticUpdaterBackend
        auBackend.GUID = GUID
        auBackend.wyUpdateCommandline = Nothing
        auBackend.wyUpdateLocation = wyUpdateLocation
        auBackend.UpdateType = UpdateType.DoNothing


        AddHandler auBackend.ReadyToBeInstalled, AddressOf auBackend_ReadyToBeInstalled
        AddHandler auBackend.UpdateSuccessful, AddressOf auBackend_UpdateSuccessful
        AddHandler auBackend.UpdateAvailable, AddressOf auBackend_UpdateAvailable
        AddHandler auBackend.UpToDate, AddressOf auBackend_UpToDate
        AddHandler auBackend.ProgressChanged, AddressOf auBackend_ProgressChanged


        auBackend.Initialize()

        _ClosingForInstall = auBackend.ClosingForInstall
    End Sub

    Private Sub CheckEveryXDays()
        If _
            ((DateTime.Now - auBackend.LastCheckDate).TotalDays >
             CheckDays & auBackend.UpdateStepOn = UpdateStepOn.Nothing) Then
            auBackend.ForceCheckForUpdate()
        End If
    End Sub

    Private Sub auBackend_ReadyToBeInstalled(sender As Object, e As EventArgs)
        If (auBackend.UpdateStepOn = UpdateStepOn.UpdateReadyToInstall) Then
            _UpadteReadyToBeInstalled = True
            RaiseEvent ReadyToBeInstalled()
        End If
    End Sub

    Private Sub auBackend_UpdateSuccessful(sender As Object, e As EventArgs)
        RaiseEvent UpdateSuccessful()
    End Sub

    Private Sub auBackend_UpdateAvailable(sender As Object, e As EventArgs)
        _UpdateAvailableToBeInstalled = True
        RaiseEvent UpdateAvailable()
    End Sub

    Private Sub auBackend_UpToDate(sender As Object, e As EventArgs)
        RaiseEvent UpdateUpToDate()
    End Sub

    Private Sub auBackend_ProgressChanged(sender As Object, progress As Integer)
        RaiseEvent ProgressChanged()
    End Sub

    Public Sub ForceCheckForUpdates()
        auBackend.ForceCheckForUpdate()
    End Sub

    Public Sub CheckForUpdates()

        If Not auBackend.ClosingForInstall Then
            CheckEveryXDays()
        End If
    End Sub

    Public Sub InstallNow()
        If (Not auBackend.UpdateStepOn = UpdateStepOn.ExtractingUpdate) Then
            auBackend.InstallNow()
        End If
    End Sub
End Class

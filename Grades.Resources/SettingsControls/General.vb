﻿Imports Grades.Localization
Imports Grades.DB.Repository

Namespace SettingsControls

    Public Class General
        Public Property ControlName As String = "general"
        Public Property GroupName As String = GeneralUI.General


        Private Sub Localize_Control()
            Me.gb_language.Text = GeneralUI.Language
            lbl_language_restart.Text = Localization.Text.settings_changesneedrestart
        End Sub


        Private Sub General_Load(sender As Object, e As EventArgs) Handles MyBase.Load

            Localize_Control()

            cb_language.Items.Add(GeneralUI.Automatically)

            Dim Language As KeyValuePair(Of String, String)
            For Each Language In Languages.Support
                cb_language.Items.Add(Language.Value)
            Next


            Dim lang = Settings.GetValue("Language")

            If IsNothing(lang) Then
                cb_language.Text = GeneralUI.Automatically
            ElseIf IsNothing(lang.Value_String) Then
                cb_language.Text = GeneralUI.Automatically
            Else

                If Languages.Support.ContainsKey(lang.Value_String) Then
                    cb_language.Text = Languages.Support.Item(lang.Value_String)
                Else
                    cb_language.Text = GeneralUI.Automatically
                End If

            End If
        End Sub

        Private Sub Event_cb_language_SelectionChangeCommitted(sender As Object, e As EventArgs) _
            Handles cb_language.SelectionChangeCommitted
            Dim Language As KeyValuePair(Of String, String)
            Settings.SetValue("Language", Nothing)
            For Each Language In Languages.Support
                If cb_language.Text = Language.Value Then
                    Settings.SetValue("Language", Language.Key)
                End If
            Next

            lbl_language_restart.Visible = True
        End Sub
    End Class
End NameSpace
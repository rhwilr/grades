﻿Imports Grades.Resources.UI

Namespace SettingsControls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Update
        Inherits System.Windows.Forms.UserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
            Me.gb_updatesoptions = New System.Windows.Forms.GroupBox()
            Me.cb_autoupdate = New System.Windows.Forms.CheckBox()
            Me.gb_updates = New System.Windows.Forms.GroupBox()
            Me.lbl_uptodate = New System.Windows.Forms.Label()
            Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
            Me.lbl_currentversion_desc = New System.Windows.Forms.Label()
            Me.lbl_availableversion_desc = New System.Windows.Forms.Label()
            Me.lbl_currentversion = New System.Windows.Forms.Label()
            Me.lbl_availableversion = New System.Windows.Forms.Label()
            Me.lbl_checkingforupdates = New System.Windows.Forms.Label()
            Me.btn_updateaction = New System.Windows.Forms.Button()
            Me.lc_updates = New Grades.Resources.UI.LoadingCircle()
            Me.FlowLayoutPanel1.SuspendLayout()
            Me.gb_updatesoptions.SuspendLayout()
            Me.gb_updates.SuspendLayout()
            Me.TableLayoutPanel1.SuspendLayout()
            Me.SuspendLayout()
            '
            'FlowLayoutPanel1
            '
            Me.FlowLayoutPanel1.Controls.Add(Me.gb_updatesoptions)
            Me.FlowLayoutPanel1.Controls.Add(Me.gb_updates)
            Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
            Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
            Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
            Me.FlowLayoutPanel1.Padding = New System.Windows.Forms.Padding(0, 0, 20, 0)
            Me.FlowLayoutPanel1.Size = New System.Drawing.Size(574, 414)
            Me.FlowLayoutPanel1.TabIndex = 0
            '
            'gb_updatesoptions
            '
            Me.gb_updatesoptions.Controls.Add(Me.cb_autoupdate)
            Me.gb_updatesoptions.Location = New System.Drawing.Point(3, 3)
            Me.gb_updatesoptions.Margin = New System.Windows.Forms.Padding(3, 3, 20, 3)
            Me.gb_updatesoptions.Name = "gb_updatesoptions"
            Me.gb_updatesoptions.Size = New System.Drawing.Size(554, 48)
            Me.gb_updatesoptions.TabIndex = 0
            Me.gb_updatesoptions.TabStop = False
            Me.gb_updatesoptions.Text = "Updates"
            '
            'cb_autoupdate
            '
            Me.cb_autoupdate.AutoSize = True
            Me.cb_autoupdate.Location = New System.Drawing.Point(6, 19)
            Me.cb_autoupdate.Name = "cb_autoupdate"
            Me.cb_autoupdate.Size = New System.Drawing.Size(284, 17)
            Me.cb_autoupdate.TabIndex = 0
            Me.cb_autoupdate.Text = "Update automatically when new updates are available."
            Me.cb_autoupdate.UseVisualStyleBackColor = True
            '
            'gb_updates
            '
            Me.gb_updates.Controls.Add(Me.lbl_uptodate)
            Me.gb_updates.Controls.Add(Me.TableLayoutPanel1)
            Me.gb_updates.Controls.Add(Me.lbl_checkingforupdates)
            Me.gb_updates.Controls.Add(Me.lc_updates)
            Me.gb_updates.Controls.Add(Me.btn_updateaction)
            Me.gb_updates.Location = New System.Drawing.Point(3, 57)
            Me.gb_updates.Margin = New System.Windows.Forms.Padding(3, 3, 20, 3)
            Me.gb_updates.Name = "gb_updates"
            Me.gb_updates.Size = New System.Drawing.Size(554, 104)
            Me.gb_updates.TabIndex = 0
            Me.gb_updates.TabStop = False
            '
            'lbl_uptodate
            '
            Me.lbl_uptodate.AutoSize = True
            Me.lbl_uptodate.Location = New System.Drawing.Point(9, 78)
            Me.lbl_uptodate.Name = "lbl_uptodate"
            Me.lbl_uptodate.Size = New System.Drawing.Size(93, 13)
            Me.lbl_uptodate.TabIndex = 6
            Me.lbl_uptodate.Text = "Already up to date"
            Me.lbl_uptodate.Visible = False
            '
            'TableLayoutPanel1
            '
            Me.TableLayoutPanel1.ColumnCount = 2
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.25926!))
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.74074!))
            Me.TableLayoutPanel1.Controls.Add(Me.lbl_currentversion_desc, 0, 0)
            Me.TableLayoutPanel1.Controls.Add(Me.lbl_availableversion_desc, 0, 1)
            Me.TableLayoutPanel1.Controls.Add(Me.lbl_currentversion, 1, 0)
            Me.TableLayoutPanel1.Controls.Add(Me.lbl_availableversion, 1, 1)
            Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 19)
            Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
            Me.TableLayoutPanel1.RowCount = 2
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
            Me.TableLayoutPanel1.Size = New System.Drawing.Size(269, 41)
            Me.TableLayoutPanel1.TabIndex = 5
            '
            'lbl_currentversion_desc
            '
            Me.lbl_currentversion_desc.AutoSize = True
            Me.lbl_currentversion_desc.Location = New System.Drawing.Point(3, 0)
            Me.lbl_currentversion_desc.Name = "lbl_currentversion_desc"
            Me.lbl_currentversion_desc.Size = New System.Drawing.Size(82, 13)
            Me.lbl_currentversion_desc.TabIndex = 0
            Me.lbl_currentversion_desc.Text = "Current Version:"
            '
            'lbl_availableversion_desc
            '
            Me.lbl_availableversion_desc.AutoSize = True
            Me.lbl_availableversion_desc.Location = New System.Drawing.Point(3, 20)
            Me.lbl_availableversion_desc.Name = "lbl_availableversion_desc"
            Me.lbl_availableversion_desc.Size = New System.Drawing.Size(91, 13)
            Me.lbl_availableversion_desc.TabIndex = 4
            Me.lbl_availableversion_desc.Text = "Available Version:"
            Me.lbl_availableversion_desc.Visible = False
            '
            'lbl_currentversion
            '
            Me.lbl_currentversion.AutoSize = True
            Me.lbl_currentversion.Location = New System.Drawing.Point(135, 0)
            Me.lbl_currentversion.Name = "lbl_currentversion"
            Me.lbl_currentversion.Size = New System.Drawing.Size(34, 13)
            Me.lbl_currentversion.TabIndex = 5
            Me.lbl_currentversion.Text = "X.X.X"
            '
            'lbl_availableversion
            '
            Me.lbl_availableversion.AutoSize = True
            Me.lbl_availableversion.Location = New System.Drawing.Point(135, 20)
            Me.lbl_availableversion.Name = "lbl_availableversion"
            Me.lbl_availableversion.Size = New System.Drawing.Size(34, 13)
            Me.lbl_availableversion.TabIndex = 6
            Me.lbl_availableversion.Text = "X.X.X"
            Me.lbl_availableversion.Visible = False
            '
            'lbl_checkingforupdates
            '
            Me.lbl_checkingforupdates.AutoSize = True
            Me.lbl_checkingforupdates.Location = New System.Drawing.Point(46, 78)
            Me.lbl_checkingforupdates.Name = "lbl_checkingforupdates"
            Me.lbl_checkingforupdates.Size = New System.Drawing.Size(110, 13)
            Me.lbl_checkingforupdates.TabIndex = 3
            Me.lbl_checkingforupdates.Text = "Checking for Updates"
            Me.lbl_checkingforupdates.Visible = False
            '
            'btn_updateaction
            '
            Me.btn_updateaction.Location = New System.Drawing.Point(11, 73)
            Me.btn_updateaction.Name = "btn_updateaction"
            Me.btn_updateaction.Size = New System.Drawing.Size(145, 23)
            Me.btn_updateaction.TabIndex = 1
            Me.btn_updateaction.Text = "Check for Updates"
            Me.btn_updateaction.UseVisualStyleBackColor = True
            '
            'lc_updates
            '
            Me.lc_updates.Active = False
            Me.lc_updates.Color = System.Drawing.Color.DarkGray
            Me.lc_updates.InnerCircleRadius = 6
            Me.lc_updates.Location = New System.Drawing.Point(14, 73)
            Me.lc_updates.Name = "lc_updates"
            Me.lc_updates.NumberSpoke = 9
            Me.lc_updates.OuterCircleRadius = 7
            Me.lc_updates.RotationSpeed = 100
            Me.lc_updates.Size = New System.Drawing.Size(26, 23)
            Me.lc_updates.SpokeThickness = 4
            Me.lc_updates.StylePreset = Grades.Resources.UI.LoadingCircle.StylePresets.Firefox
            Me.lc_updates.TabIndex = 2
            Me.lc_updates.Text = "LoadingCircle1"
            Me.lc_updates.Visible = False
            '
            'Update
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.FlowLayoutPanel1)
            Me.Name = "Update"
            Me.Size = New System.Drawing.Size(580, 420)
            Me.FlowLayoutPanel1.ResumeLayout(False)
            Me.gb_updatesoptions.ResumeLayout(False)
            Me.gb_updatesoptions.PerformLayout()
            Me.gb_updates.ResumeLayout(False)
            Me.gb_updates.PerformLayout()
            Me.TableLayoutPanel1.ResumeLayout(False)
            Me.TableLayoutPanel1.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
        Friend WithEvents gb_updates As System.Windows.Forms.GroupBox
        Friend WithEvents lc_updates As LoadingCircle
        Friend WithEvents btn_updateaction As System.Windows.Forms.Button
        Friend WithEvents lbl_currentversion_desc As System.Windows.Forms.Label
        Friend WithEvents lbl_checkingforupdates As System.Windows.Forms.Label
        Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
        Friend WithEvents lbl_availableversion_desc As System.Windows.Forms.Label
        Friend WithEvents lbl_currentversion As System.Windows.Forms.Label
        Friend WithEvents lbl_availableversion As System.Windows.Forms.Label
        Friend WithEvents gb_updatesoptions As System.Windows.Forms.GroupBox
        Friend WithEvents lbl_uptodate As System.Windows.Forms.Label
        Friend WithEvents cb_autoupdate As System.Windows.Forms.CheckBox

    End Class
End NameSpace
﻿Namespace SettingsControls
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class General
        Inherits System.Windows.Forms.UserControl

        'UserControl overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
            Me.gb_language = New System.Windows.Forms.GroupBox()
            Me.lbl_language_restart = New System.Windows.Forms.Label()
            Me.cb_language = New System.Windows.Forms.ComboBox()
            Me.FlowLayoutPanel1.SuspendLayout()
            Me.gb_language.SuspendLayout()
            Me.SuspendLayout()
            '
            'FlowLayoutPanel1
            '
            Me.FlowLayoutPanel1.Controls.Add(Me.gb_language)
            Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
            Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 3)
            Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
            Me.FlowLayoutPanel1.Padding = New System.Windows.Forms.Padding(0, 0, 20, 0)
            Me.FlowLayoutPanel1.Size = New System.Drawing.Size(574, 414)
            Me.FlowLayoutPanel1.TabIndex = 0
            '
            'gb_language
            '
            Me.gb_language.Controls.Add(Me.lbl_language_restart)
            Me.gb_language.Controls.Add(Me.cb_language)
            Me.gb_language.Location = New System.Drawing.Point(3, 3)
            Me.gb_language.Margin = New System.Windows.Forms.Padding(3, 3, 20, 3)
            Me.gb_language.Name = "gb_language"
            Me.gb_language.Size = New System.Drawing.Size(554, 64)
            Me.gb_language.TabIndex = 0
            Me.gb_language.TabStop = False
            Me.gb_language.Text = "Language"
            '
            'lbl_language_restart
            '
            Me.lbl_language_restart.AutoSize = True
            Me.lbl_language_restart.Location = New System.Drawing.Point(6, 43)
            Me.lbl_language_restart.Name = "lbl_language_restart"
            Me.lbl_language_restart.Size = New System.Drawing.Size(104, 13)
            Me.lbl_language_restart.TabIndex = 1
            Me.lbl_language_restart.Text = "Restart after change"
            Me.lbl_language_restart.Visible = False
            '
            'cb_language
            '
            Me.cb_language.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.cb_language.FormattingEnabled = True
            Me.cb_language.Location = New System.Drawing.Point(6, 19)
            Me.cb_language.Name = "cb_language"
            Me.cb_language.Size = New System.Drawing.Size(209, 21)
            Me.cb_language.TabIndex = 0
            '
            'General
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.Controls.Add(Me.FlowLayoutPanel1)
            Me.Name = "General"
            Me.Size = New System.Drawing.Size(580, 420)
            Me.FlowLayoutPanel1.ResumeLayout(False)
            Me.gb_language.ResumeLayout(False)
            Me.gb_language.PerformLayout()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
        Friend WithEvents gb_language As System.Windows.Forms.GroupBox
        Friend WithEvents cb_language As System.Windows.Forms.ComboBox
        Friend WithEvents lbl_language_restart As System.Windows.Forms.Label

    End Class
End NameSpace
﻿Imports Grades.Localization
Imports Grades.Update
Imports Grades.DB.Repository

Namespace SettingsControls

    Public Class Update
        Public Property ControlName As String = "update"
        Public Property GroupName As String = GeneralUI.Updates

        Private WithEvents Grades_Updater As New auUpdater

        Private Sub Localize_Control()
            Me.gb_updatesoptions.Text = GeneralUI.Updates
            lbl_currentversion_desc.Text = GeneralUI.currentversion
            lbl_availableversion_desc.Text = GeneralUI.availableversion
            lbl_uptodate.Text = Localization.Text.settings_uptodate
            cb_autoupdate.Text = Localization.Text.settings_autoupdate
        End Sub


        Private Sub General_Load(sender As Object, e As EventArgs) Handles MyBase.Load

            Localize_Control()

            Dim autoupdate = Settings.GetValue("AutoUpdate")

            If IsNothing(autoupdate) Then
                cb_autoupdate.Checked = False
            ElseIf IsNothing(autoupdate.Value_Boolean) Then
                cb_autoupdate.Checked = False
            Else
                cb_autoupdate.Checked = autoupdate.Value_Boolean
            End If


            lbl_currentversion.Text = String.Format("Version {0}.{1}.{2}", My.Application.Info.Version.Major,
                                                    My.Application.Info.Version.Minor, My.Application.Info.Version.Build)

            If Grades_Updater.UpdateAvailableToBeInstalled Then
                lbl_availableversion_desc.Visible = True
                lbl_availableversion.Visible = True

                btn_updateaction.Text = Localization.Text.settings_installupdates
            Else
                lbl_availableversion_desc.Visible = False
                lbl_availableversion.Visible = False

                btn_updateaction.Text = Localization.Text.settings_checkforupdates
            End If
        End Sub


        Private Sub btn_updateaction_Click(sender As Object, e As EventArgs) Handles btn_updateaction.Click


            If Grades_Updater.UpdateAvailableToBeInstalled Or Grades_Updater.UpdateReadyToBeInstalled Then

                btn_updateaction.Visible = False

                lbl_checkingforupdates.Visible = True
                lbl_checkingforupdates.Text = Localization.Text.settings_installingupdates

                lc_updates.Visible = True
                lc_updates.Active = True

                Grades_Updater.InstallNow()
            Else
                btn_updateaction.Visible = False

                lbl_checkingforupdates.Visible = True
                lbl_checkingforupdates.Text = Localization.Text.settings_checkingforupdates

                lc_updates.Visible = True
                lc_updates.Active = True

                Grades_Updater.ForceCheckForUpdates()
            End If
        End Sub

        Private Delegate Sub Grades_Updater_UpdateDelegate()

        Private Sub Grades_Updater_UpdateAvailable() Handles Grades_Updater.UpdateAvailable

            If Me.InvokeRequired Then
                Me.Invoke(New Grades_Updater_UpdateDelegate(AddressOf Grades_Updater_UpdateAvailable))
            Else

                btn_updateaction.Visible = True
                btn_updateaction.Text = Localization.Text.settings_installupdates

                lbl_checkingforupdates.Visible = False

                lc_updates.Visible = False
                lc_updates.Active = False

                lbl_availableversion_desc.Visible = True
                lbl_availableversion.Visible = True
                lbl_availableversion.Text = Grades_Updater.Version
            End If
        End Sub

        Private Sub Grades_Updater_UpdateUpToDate() Handles Grades_Updater.UpdateUpToDate

            If Me.InvokeRequired Then
                Me.Invoke(New Grades_Updater_UpdateDelegate(AddressOf Grades_Updater_UpdateUpToDate))
            Else

                btn_updateaction.Visible = False

                lbl_checkingforupdates.Visible = False

                lc_updates.Visible = False
                lc_updates.Active = False

                lbl_availableversion_desc.Visible = False
                lbl_availableversion.Visible = False
                lbl_availableversion.Text = Grades_Updater.Version

                lbl_uptodate.Visible = True
            End If
        End Sub

        Private Sub Grades_Updater_ReadyToBeInstalled() Handles Grades_Updater.ReadyToBeInstalled
            Grades_Updater.InstallNow()
        End Sub

        Private Sub cb_autoupdate_CheckedChanged(sender As Object, e As EventArgs) Handles cb_autoupdate.CheckedChanged
            Settings.SetValue("AutoUpdate", cb_autoupdate.Checked)
        End Sub
    End Class
End NameSpace
﻿Partial Public Class SplashScreen
    Private Delegate Sub UpdateProgressDelegate(ByVal status As String)

    Public Sub UpdateProgress(ByVal status As String)
        If Me.InvokeRequired Then
            Me.Invoke(New UpdateProgressDelegate(AddressOf UpdateProgress), New Object() {status})
        Else
            CompseSplashScreen(status)
        End If
    End Sub


    Private Sub CompseSplashScreen(Optional status As String = Nothing)

        Dim Color As Color = Color.FromArgb(CType(CType(211, Byte), Integer), CType(CType(211, Byte), Integer),
                                            CType(CType(211, Byte), Integer))

        Dim Image As Image = My.Resources.Resources.Grades_SplashScreen

        Image = WriteText(Image, "Beta", 10, FontStyle.Bold, Color, New Point(140, 290))
        Image = WriteText(Image,
                          String.Format("Version {0}.{1}.{2}", My.Application.Info.Version.Major,
                                        My.Application.Info.Version.Minor, My.Application.Info.Version.Build), 10,
                          FontStyle.Regular,
                          Color, New Point(180, 290))
        Image = WriteText(Image, String.Format("{0}", My.Application.Info.Copyright.ToString), 8, FontStyle.Regular,
                          Color, New Point(140, 310))

        If Not status = Nothing Then
            Image = WriteText(Image, status, 8, FontStyle.Regular, Color, New Point(300, 210))
        End If

        UpdateFormDisplay(Image)
    End Sub
End Class

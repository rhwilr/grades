Imports System.Runtime.InteropServices

Friend Class API
    Public Const AC_SRC_OVER As Byte = &H0
    Public Const AC_SRC_ALPHA As Byte = &H1
    Public Const ULW_ALPHA As Int32 = &H2

    <StructLayout(LayoutKind.Sequential, Pack := 1)>
    Public Structure BLENDFUNCTION
        Public BlendOp As Byte
        Public BlendFlags As Byte
        Public SourceConstantAlpha As Byte
        Public AlphaFormat As Byte
    End Structure

    Public Declare Auto Function UpdateLayeredWindow Lib "user32.dll"(hwnd As IntPtr, hdcDst As IntPtr,
                                                                      ByRef pptDst As Point, ByRef psize As Size,
                                                                      hdcSrc As IntPtr, ByRef pprSrc As Point,
                                                                      crKey As Int32, ByRef pblend As BLENDFUNCTION,
                                                                      dwFlags As Int32) As Boolean


    Public Declare Auto Function GetDC Lib "user32.dll"(hWnd As IntPtr) As IntPtr

    Public Declare Auto Function CreateCompatibleDC Lib "gdi32.dll"(hDC As IntPtr) As IntPtr

    <DllImport("user32.dll", ExactSpelling := True)>
    Public Shared Function ReleaseDC(hWnd As IntPtr, hDC As IntPtr) As Integer
    End Function

    Public Declare Auto Function DeleteDC Lib "gdi32.dll"(hdc As IntPtr) As Boolean


    <DllImport("gdi32.dll", ExactSpelling := True)>
    Public Shared Function SelectObject(hDC As IntPtr, hObject As IntPtr) As IntPtr
    End Function

    Public Declare Auto Function DeleteObject Lib "gdi32.dll"(hObject As IntPtr) As Boolean
End Class
﻿
Imports System.Drawing.Drawing2D


Partial Public Class SplashScreen
    Inherits Form

    Public Sub New()
        InitializeComponent()
    End Sub

#Region "CUSTOM PAINT METHODS ----------------------------------------------"

    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ExStyle = cp.ExStyle Or &H80000
            ' Required: set WS_EX_LAYERED extended style
            Return cp
        End Get
    End Property

    'Updates the Form's display using API calls
    Private Sub UpdateFormDisplay(backgroundImage As Image)
        Dim screenDc As IntPtr = API.GetDC(IntPtr.Zero)
        Dim memDc As IntPtr = API.CreateCompatibleDC(screenDc)
        Dim hBitmap As IntPtr = IntPtr.Zero
        Dim oldBitmap As IntPtr = IntPtr.Zero

        Try
            'Display-image
            Dim bmp As New Bitmap(backgroundImage)
            hBitmap = bmp.GetHbitmap(Color.FromArgb(0))
            'Set the fact that background is transparent
            oldBitmap = API.SelectObject(memDc, hBitmap)

            'Display-rectangle
            Dim size As Size = bmp.Size
            Dim pointSource As New Point(0, 0)
            Dim topPos As New Point(Me.Left, Me.Top)

            'Set up blending options
            Dim blend As New API.BLENDFUNCTION()
            blend.BlendOp = API.AC_SRC_OVER
            blend.BlendFlags = 0
            blend.SourceConstantAlpha = 255
            blend.AlphaFormat = API.AC_SRC_ALPHA

            API.UpdateLayeredWindow(Me.Handle, screenDc, topPos, size, memDc, pointSource,
                                    0, blend, API.ULW_ALPHA)

            'Clean-up
            bmp.Dispose()
            API.ReleaseDC(IntPtr.Zero, screenDc)
            If hBitmap <> IntPtr.Zero Then
                API.SelectObject(memDc, oldBitmap)
                API.DeleteObject(hBitmap)
            End If
            API.DeleteDC(memDc)
        Catch generatedExceptionName As Exception
        End Try
    End Sub

#End Region


#Region "FORM EVENTS -------------------------------------------------------"

    Private Sub SplashScreen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CompseSplashScreen
    End Sub


    Private Sub SplashScreen_Paint(sender As Object, e As PaintEventArgs) Handles MyBase.Paint
        UpdateFormDisplay(Me.BackgroundImage)
    End Sub

#End Region


#Region "Generate Background -----------------------------------------"

    Private Function WriteText(Image As Image, drawString As String, FontSize As Integer, FontStyle As FontStyle,
                               Color As Color, drawPoint As Point) As Bitmap

        Dim mybitMap As New Bitmap(Image)
        Dim myGR As Graphics = Graphics.FromImage(mybitMap)
        myGR.SmoothingMode = SmoothingMode.AntiAlias
        Dim drawFont As New Font("Microsoft Sans Serif", FontSize, FontStyle)
        Dim drawBrush As New SolidBrush(Color)
        myGR.DrawString(drawString, drawFont, drawBrush, drawPoint)
        Return mybitMap
    End Function

#End Region

    Private Sub SplashScreen_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        e.Cancel = True
    End Sub
End Class
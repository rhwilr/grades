﻿Namespace Forms.Settings
    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
    Partial Class Settings
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> _
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> _
        Private Sub InitializeComponent()
            Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
            Me.pnl_Settings_Controls = New System.Windows.Forms.Panel()
            Me.pnl_Settings_Title = New System.Windows.Forms.Panel()
            Me.lbl_Settings_Title = New System.Windows.Forms.Label()
            Me.dgv_SettingsGrid = New System.Windows.Forms.DataGridView()
            Me.dgc_school_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.dgc_school_school = New System.Windows.Forms.DataGridViewTextBoxColumn()
            Me.btn_Setting_ok = New System.Windows.Forms.Button()
            Me.pnl_Settings_Title.SuspendLayout()
            CType(Me.dgv_SettingsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'pnl_Settings_Controls
            '
            Me.pnl_Settings_Controls.Location = New System.Drawing.Point(226, 45)
            Me.pnl_Settings_Controls.Name = "pnl_Settings_Controls"
            Me.pnl_Settings_Controls.Size = New System.Drawing.Size(574, 428)
            Me.pnl_Settings_Controls.TabIndex = 1
            '
            'pnl_Settings_Title
            '
            Me.pnl_Settings_Title.Controls.Add(Me.lbl_Settings_Title)
            Me.pnl_Settings_Title.Location = New System.Drawing.Point(226, 13)
            Me.pnl_Settings_Title.Name = "pnl_Settings_Title"
            Me.pnl_Settings_Title.Size = New System.Drawing.Size(574, 26)
            Me.pnl_Settings_Title.TabIndex = 2
            '
            'lbl_Settings_Title
            '
            Me.lbl_Settings_Title.AutoSize = True
            Me.lbl_Settings_Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.lbl_Settings_Title.Location = New System.Drawing.Point(3, 0)
            Me.lbl_Settings_Title.Name = "lbl_Settings_Title"
            Me.lbl_Settings_Title.Size = New System.Drawing.Size(35, 18)
            Me.lbl_Settings_Title.TabIndex = 0
            Me.lbl_Settings_Title.Text = "Title"
            '
            'dgv_SettingsGrid
            '
            Me.dgv_SettingsGrid.AllowUserToAddRows = False
            Me.dgv_SettingsGrid.AllowUserToDeleteRows = False
            Me.dgv_SettingsGrid.AllowUserToResizeColumns = False
            Me.dgv_SettingsGrid.AllowUserToResizeRows = False
            Me.dgv_SettingsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
            Me.dgv_SettingsGrid.BackgroundColor = System.Drawing.Color.White
            Me.dgv_SettingsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
            Me.dgv_SettingsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
            Me.dgv_SettingsGrid.ColumnHeadersVisible = False
            Me.dgv_SettingsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgc_school_id, Me.dgc_school_school})
            Me.dgv_SettingsGrid.Location = New System.Drawing.Point(12, 12)
            Me.dgv_SettingsGrid.MultiSelect = False
            Me.dgv_SettingsGrid.Name = "dgv_SettingsGrid"
            Me.dgv_SettingsGrid.ReadOnly = True
            Me.dgv_SettingsGrid.RowHeadersVisible = False
            Me.dgv_SettingsGrid.RowTemplate.Height = 30
            Me.dgv_SettingsGrid.Size = New System.Drawing.Size(208, 461)
            Me.dgv_SettingsGrid.TabIndex = 3
            '
            'dgc_school_id
            '
            Me.dgc_school_id.HeaderText = "ID"
            Me.dgc_school_id.Name = "dgc_school_id"
            Me.dgc_school_id.ReadOnly = True
            Me.dgc_school_id.Visible = False
            '
            'dgc_school_school
            '
            DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
            DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            DataGridViewCellStyle1.Padding = New System.Windows.Forms.Padding(3)
            Me.dgc_school_school.DefaultCellStyle = DataGridViewCellStyle1
            Me.dgc_school_school.HeaderText = "SettingGroup"
            Me.dgc_school_school.Name = "dgc_school_school"
            Me.dgc_school_school.ReadOnly = True
            '
            'btn_Setting_ok
            '
            Me.btn_Setting_ok.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.btn_Setting_ok.Location = New System.Drawing.Point(725, 479)
            Me.btn_Setting_ok.Name = "btn_Setting_ok"
            Me.btn_Setting_ok.Size = New System.Drawing.Size(75, 23)
            Me.btn_Setting_ok.TabIndex = 4
            Me.btn_Setting_ok.Text = "OK"
            Me.btn_Setting_ok.UseVisualStyleBackColor = True
            '
            'Settings
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(812, 514)
            Me.Controls.Add(Me.btn_Setting_ok)
            Me.Controls.Add(Me.dgv_SettingsGrid)
            Me.Controls.Add(Me.pnl_Settings_Title)
            Me.Controls.Add(Me.pnl_Settings_Controls)
            Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
            Me.MaximizeBox = False
            Me.MinimizeBox = False
            Me.Name = "Settings"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
            Me.Text = "Settings"
            Me.pnl_Settings_Title.ResumeLayout(False)
            Me.pnl_Settings_Title.PerformLayout()
            CType(Me.dgv_SettingsGrid, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub
        Friend WithEvents pnl_Settings_Controls As System.Windows.Forms.Panel
        Friend WithEvents pnl_Settings_Title As System.Windows.Forms.Panel
        Friend WithEvents dgv_SettingsGrid As System.Windows.Forms.DataGridView
        Friend WithEvents dgc_school_id As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents dgc_school_school As System.Windows.Forms.DataGridViewTextBoxColumn
        Friend WithEvents lbl_Settings_Title As System.Windows.Forms.Label
        Friend WithEvents btn_Setting_ok As System.Windows.Forms.Button
    End Class
End NameSpace
﻿
Imports Grades.Resources
Imports Grades.Resources.SettingsControls

Namespace Forms.Settings
    Partial Public Class Settings
        Private ReadOnly SettingControl As New List(Of Object)

        Private Sub Data_LoadControls()

            ' SettingControl.Add(New Grades.Resources.noControl)
            SettingControl.Add(New General)
            SettingControl.Add(New SettingsControls.Update)

            For Each page In SettingControl
                page.Dock = DockStyle.Fill
                page.Location = New Point(0, 0)
                page.Size = New Size(580, 420)

                dgv_SettingsGrid.Rows.Add(page.ControlName, page.GroupName)
            Next
        End Sub
    End Class
End Namespace
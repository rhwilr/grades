﻿Namespace Forms.Settings
    Partial Public Class Settings
        Private Sub Event_SettingsGrid_SelectionChanged(sender As Object, e As EventArgs) _
            Handles dgv_SettingsGrid.SelectionChanged

            pnl_Settings_Controls.Controls.Clear()

            Me.pnl_Settings_Controls.Controls.Add(SettingControl.Item(dgv_SettingsGrid.CurrentRow.Index))
            Me.lbl_Settings_Title.Text = SettingControl.Item(dgv_SettingsGrid.CurrentRow.Index).GroupName
        End Sub
    End Class
End NameSpace
﻿Imports Grades.DB.Repository

Public Class Main
    Protected Friend Sub Data_Ribbon_Orb_SchoolRecent(Optional CheckedID As Integer = Nothing)

        Ribbon.OrbDropDown.RecentItems.Clear()

        For Each _Schools In _
            (From school In Connection.GradesContext.School).OrderBy(Function(school) school.Identifier)

            Dim rr_orb_item As New RibbonOrbRecentItem
            rr_orb_item.Text = _Schools.Identifier
            rr_orb_item.Tag = "rr_orb_:" & _Schools.Identifier
            rr_orb_item.Value = _Schools.ID
            rr_orb_item.CheckOnClick = True
            Ribbon.OrbDropDown.RecentItems.Add(rr_orb_item)

            If _Schools.ID = CheckedID Then
                rr_orb_item.Checked = True
            End If

            AddHandler rr_orb_item.Click, AddressOf Event_Ribbon_Orb_RibbonOrbRecentItem_Click
        Next
    End Sub
End Class

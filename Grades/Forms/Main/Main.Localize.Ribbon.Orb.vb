﻿Imports Grades.Localization

Partial Public Class Main
    Private Sub Localize_Ribbon_Orb()

        Me.Ribbon.OrbText = GeneralUI.File
        Me.Ribbon.OrbDropDown.RecentItemsCaption = GeneralUI.Schools
        Me.rb_orb_options_Settings.Text = GeneralUI.Settings
        Me.rb_orb_menu_School_Manager.Text = GeneralUI.School_Manager
        Me.rb_orb_options_About.Text = GeneralUI.About
    End Sub
End Class

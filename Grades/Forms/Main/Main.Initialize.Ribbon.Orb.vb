﻿Imports Grades.Resources

Partial Public Class Main
    Private Sub Initialize_Ribbon_Orb_Icons()

        Me.rb_orb_menu_School_Manager.Image = Functions.Resources.ResizeImage(RibbonIcons.Arithmetic, 32)
        Me.rb_orb_menu_School_Manager.SmallImage = Functions.Resources.ResizeImage(RibbonIcons.Arithmetic, 32)

        Me.rb_orb_options_Settings.Image = Functions.Resources.ResizeImage(RibbonIcons.Settings, 32)
        Me.rb_orb_options_Settings.SmallImage = Functions.Resources.ResizeImage(RibbonIcons.Settings, 16)

        Me.rb_orb_options_About.Image = Functions.Resources.ResizeImage(RibbonIcons.Information, 32)
        Me.rb_orb_options_About.SmallImage = Functions.Resources.ResizeImage(RibbonIcons.Information, 16)
    End Sub
End Class

﻿
Imports Grades.Update
Imports System.Threading
Imports Grades.DB.Repository

Partial Public Class Main
    Private ReadOnly splash As SplashScreen = CType(My.Application.SplashScreen, SplashScreen)

    Private WithEvents Grades_Updater As New auUpdater

    Private Sub Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        splash.UpdateProgress("Loading Assemblies")

        Dim autoupdate = Settings.GetValue("AutoUpdate")
        Dim _autoupdate As Boolean
        If IsNothing(autoupdate) Then
            _autoupdate = False
        ElseIf IsNothing(autoupdate.Value_Boolean) Then
            _autoupdate = False
        Else
            _autoupdate = autoupdate.Value_Boolean
        End If
        
        If _autoupdate Then
            
            splash.UpdateProgress("Checking for Updates...")

            Grades_Updater.CheckForUpdates()

            While Grades_Updater.UpdateStepOnString = "Checking"
                Thread.Sleep(50)
            End While

        End If

        If Grades_Updater.UpdateStepOnString = "UpdateAvailable" Then

            Grades_Updater.InstallNow()

            While Grades_Updater.UpdateStepOnString = "UpdateAvailable"
                Thread.Sleep(50)
            End While
            WindowState = FormWindowState.Minimized

        Else

            splash.UpdateProgress("Loading Languages...")
            Localize()


#If DEBUG Then

            splash.UpdateProgress("Filling DB with Debug Records...")

            Grades.Functions.Debug.Debug()
#End If

            splash.UpdateProgress("Loading Ribbon...")
            Initialize_Ribbon()

        End If

    End Sub


    Private Sub Grades_Updater_ReadyToBeInstalled() Handles Grades_Updater.ReadyToBeInstalled
        Grades_Updater.InstallNow()
    End Sub
End Class

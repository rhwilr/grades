﻿Imports Grades.DB.Repository
Imports Grades.Localization

Partial Public Class Main
    Private Sub Data_Ribbon_Tab_SubjectsTabs(Optional School_ID As Integer = Nothing)

        Ribbon.Tabs.Clear()

        If _
            School_ID = Nothing Or
            (From subject In Connection.GradesContext.Subject Where subject.School.ID = School_ID).Count() = 0 Then
            Dim rt_subject As New RibbonTab
            rt_subject.Text = GeneralUI.No_Subject
            rt_subject.Tag = "rt_subject_:" & "No Subject"
            rt_subject.Value = "-"
            Ribbon.Tabs.Add(rt_subject)
        Else

            For Each _Subjects In _
                (From subject In Connection.GradesContext.Subject Where subject.School.ID = School_ID).OrderBy(
                    Function(subject) subject.Identifier)

                Dim rt_subject As New RibbonTab
                Dim rp_new As New RibbonPanel
                Dim rb_grade_add As New RibbonButton
                Dim rb_grade_edit As New RibbonButton
                Dim rb_grade_delete As New RibbonButton

                Dim rp_term As New RibbonPanel
                Dim rc_term_term As New RibbonComboBox

                rt_subject.Text = _Subjects.Identifier
                rt_subject.Tag = "rt_subject_:" & _Subjects.Identifier
                rt_subject.Value = _Subjects.ID
                Ribbon.Tabs.Add(rt_subject)


                rp_new.ButtonMoreEnabled = False
                rp_new.ButtonMoreVisible = False
                rp_new.Items.Add(rb_grade_add)
                rp_new.Items.Add(rb_grade_edit)
                rp_new.Items.Add(rb_grade_delete)
                rp_new.Text = GeneralUI.Grade
                rp_new.Tag = "rp_new_:" & _Subjects.Identifier


                rb_grade_add.Text = GeneralUI.Add
                rb_grade_add.Tag = "rb_grade_add_:" & _Subjects.Identifier
                rb_grade_add.MinSizeMode = RibbonElementSizeMode.Large

                rb_grade_edit.MaxSizeMode = RibbonElementSizeMode.Medium
                rb_grade_edit.MinSizeMode = RibbonElementSizeMode.Medium
                rb_grade_edit.Text = GeneralUI.Edit
                rb_grade_edit.Tag = "rb_grade_edit_:" & _Subjects.Identifier

                rb_grade_delete.MaxSizeMode = RibbonElementSizeMode.Medium
                rb_grade_delete.MinSizeMode = RibbonElementSizeMode.Medium
                rb_grade_delete.Text = GeneralUI.Delete
                rb_grade_delete.Tag = "rb_grade_delete_:" & _Subjects.Identifier


                rp_term.ButtonMoreEnabled = False
                rp_term.ButtonMoreVisible = False
                rp_term.Items.Add(rc_term_term)
                rp_term.Text = GeneralUI.Term
                rp_term.Tag = "rp_term_:" & _Subjects.Identifier


                rc_term_term.DrawIconsBar = False
                rc_term_term.AllowTextEdit = False
                rc_term_term.Tag = "rc_term_term_:" & _Subjects.ID
                Data_Ribbon_Tab_Term_TermItems(rc_term_term, School_ID)


                AddHandler rt_subject.ActiveChanged, AddressOf Event_Ribbon_Tab_ActiveChanged


                AddHandler rb_grade_add.Click, AddressOf Event_Ribbon_Tab_Grades_RibbonButtonAdd_Click
                AddHandler rb_grade_edit.Click, AddressOf Event_Ribbon_Tab_Grades_RibbonButtonEdit_Click
                AddHandler rb_grade_delete.Click, AddressOf Event_Ribbon_Tab_Grades_RibbonButtonDelete_Click


                AddHandler rc_term_term.TextBoxTextChanged,
                    AddressOf Event_Ribbon_Tab_Term_RibonComboBoxTextBoxTextChanged


                rt_subject.Panels.Add(rp_new)
                rt_subject.Panels.Add(rp_term)

            Next

            Data_GradesGrid_SubjectGrade_Refresh()

        End If

        Initialize_Ribbon_Tab_Icons()
    End Sub
End Class

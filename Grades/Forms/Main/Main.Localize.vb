﻿Imports System.Globalization
Imports Grades.DB.Repository
Imports System.Threading

Partial Public Class Main
    Private Sub Localize()

        Dim lang = Settings.GetValue("Language")

        If Not IsNothing(lang) Then
            If Not IsNothing(lang.Value_String) Then
                Thread.CurrentThread.CurrentUICulture = New CultureInfo((lang.Value_String).ToString)
            End If
        Else

        End If

        Localize_Main()
    End Sub
End Class

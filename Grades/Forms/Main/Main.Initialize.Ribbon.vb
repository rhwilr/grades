﻿
Imports Grades.DB.Repository

Partial Public Class Main
    Private Sub Initialize_Ribbon()

        Dim dayofweek As Integer = Date.Today.DayOfWeek - 1

        Dim _schoolday =
                (From schoolday In Connection.GradesContext.Schoolday Where schoolday.Schoolday1 = dayofweek).
                FirstOrDefault()

        If IsNothing(_schoolday) Then

            Dim _schoolday1 =
                    (From schoolday In Connection.GradesContext.Schoolday Where schoolday.Schoolday1 < dayofweek).
                    FirstOrDefault()
            Dim _schoolday2 =
                    (From schoolday In Connection.GradesContext.Schoolday Where schoolday.Schoolday1 > dayofweek).
                    FirstOrDefault()


            If Not IsNothing(_schoolday1) And Not IsNothing(_schoolday2) Then

                Dim diff_schoolday1 = Math.Abs(CInt(_schoolday1.Schoolday1) - dayofweek)
                Dim diff_schoolday2 = Math.Abs(CInt(_schoolday2.Schoolday1) - dayofweek)

                If diff_schoolday1 > diff_schoolday2 Then
                    _schoolday = _schoolday2
                Else
                    _schoolday = _schoolday1
                End If

            End If

        End If

        If Not IsNothing(_schoolday) Then
            Data_Ribbon_Orb_SchoolRecent(_schoolday.School.ID)
            Data_Ribbon_Tab_SubjectsTabs(_schoolday.School.ID)
        Else
            Data_Ribbon_Orb_SchoolRecent()
            Data_Ribbon_Tab_SubjectsTabs()
        End If


        Initialize_Ribbon_Orb_Icons()
    End Sub
End Class

﻿Imports Grades.DB.Repository

Partial Public Class Main
    Private Sub Data_GradesGrid_SubjectGrades(Optional SubjectID As Integer = Nothing)

        dgv_GradesGrid.Rows.Clear()

        For Each _Grade In _
            (From grade In Connection.GradesContext.Grade Where grade.Subject.ID = SubjectID).OrderBy(
                Function(grade) grade.Date)

            dgv_GradesGrid.Rows.Add(_Grade.ID, _Grade.Identifier, _Grade.Grade1, _Grade.Weight, _Grade.Date,
                                    _Grade.Comment)
        Next
    End Sub

    Private Sub Data_GradesGrid_SubjectGrades(SubjectID As Integer, TermID As Integer)

        dgv_GradesGrid.Rows.Clear()

        For Each _Grade In _
            (From grade In Connection.GradesContext.Grade Where grade.Subject.ID = SubjectID And grade.Term.ID = TermID) _
                .OrderBy(
                    Function(grade) grade.Date)

            dgv_GradesGrid.Rows.Add(_Grade.ID, _Grade.Identifier, _Grade.Grade1, _Grade.Weight, _Grade.Date,
                                    _Grade.Comment)
        Next
    End Sub

    Private Sub Data_GradesGrid_SubjectGrade_Refresh()

        Dim SelectedComboBoxItem As RibbonComboBox =
                Functions.Controls.FindRibbonComboBox("rc_term_term_:" & Ribbon.ActiveTab.Value, Ribbon).FirstOrDefault

        If (Not IsNothing(SelectedComboBoxItem)) Then
            Data_GradesGrid_SubjectGrades(Ribbon.ActiveTab.Value, SelectedComboBoxItem.SelectedValue)
        Else
            Data_GradesGrid_SubjectGrades(Ribbon.ActiveTab.Value)
        End If
    End Sub
End Class

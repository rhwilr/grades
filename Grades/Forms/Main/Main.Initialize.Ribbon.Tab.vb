﻿Imports Grades.Resources

Partial Public Class Main
    Private Sub Initialize_Ribbon_Tab_Icons()

        For Each RibbonItem In Functions.Controls.FindRibbonButton("rb_grade_add_*", Me.Ribbon)
            RibbonItem.Image = Functions.Resources.ResizeImage(RibbonIcons.Add, 32)
            RibbonItem.SmallImage = Functions.Resources.ResizeImage(RibbonIcons.Add, 16)
        Next

        For Each RibbonItem In Functions.Controls.FindRibbonButton("rb_grade_edit_*", Me.Ribbon)
            RibbonItem.Image = Functions.Resources.ResizeImage(RibbonIcons.Edit, 32)
            RibbonItem.SmallImage = Functions.Resources.ResizeImage(RibbonIcons.Edit, 16)
        Next

        For Each RibbonItem In Functions.Controls.FindRibbonButton("rb_grade_delete_*", Me.Ribbon)
            RibbonItem.Image = Functions.Resources.ResizeImage(RibbonIcons.Delete, 32)
            RibbonItem.SmallImage = Functions.Resources.ResizeImage(RibbonIcons.Delete, 16)
        Next
    End Sub
End Class

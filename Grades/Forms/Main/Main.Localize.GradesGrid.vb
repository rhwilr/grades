﻿Imports Grades.Localization

Partial Public Class Main
    Private Sub Localize_GradesGrid()

        Me.dgc_grades_grade.HeaderText = GeneralUI.Grade
        Me.dgc_grades_exam.HeaderText = GeneralUI.Exam
        Me.dgc_grades_weight.HeaderText = GeneralUI.Weight
        Me.dgc_grades_Date.HeaderText = GeneralUI.Datum
        Me.dgc_grades_comment.HeaderText = GeneralUI.Comment
    End Sub
End Class

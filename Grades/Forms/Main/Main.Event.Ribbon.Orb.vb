﻿Imports Grades.Forms.Settings

Partial Public Class Main
    Private Sub Event_Ribbon_Orb_RibbonOrbMenuItem_Manager_Click(sender As Object, e As EventArgs) _
        Handles rb_orb_menu_School_Manager.Click

        Dim frm_schoolmanager As New School_Manager
        frm_schoolmanager.ShowDialog()
    End Sub

    Private Sub Event_Ribbon_Orb_RibbonOrbRecentItem_Click(sender As Object, e As EventArgs)

        For Each RibbonItem In Functions.Controls.FindRibbonOrbRecentItem("rr_orb_*", Me.Ribbon)
            RibbonItem.Checked = False
        Next

        sender.Checked = True
        Dim School_ID As Integer = sender.Value

        Data_Ribbon_Tab_SubjectsTabs(School_ID)
    End Sub


    Private Sub Event_Ribbon_Orb_RibbonOrbOptionsItem_Settings_Click(sender As Object, e As EventArgs) _
        Handles rb_orb_options_Settings.Click

        Dim frm_settings As New Settings
        frm_settings.ShowDialog()
    End Sub

    Private Sub Event_Ribbon_Orb_RibbonOrbOptionsItem_About_Click(sender As Object, e As EventArgs) _
        Handles rb_orb_options_About.Click

        Dim frm_about As New About
        frm_about.ShowDialog()
    End Sub
End Class

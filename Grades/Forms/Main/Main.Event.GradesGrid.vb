﻿Imports Grades.Functions
Imports Grades.DB.Model
Imports Grades.DB.Repository


Partial Public Class Main
    Private EditingCell As Integer

    Private Sub Data_GradesGrid_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) _
        Handles dgv_GradesGrid.CellBeginEdit
        EditingCell = dgv_GradesGrid.CurrentCell.RowIndex
    End Sub

    Private Sub Data_GradesGrid_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_GradesGrid.CellEndEdit

        If _
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing And
            Not dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then

            Dim grade As New Grade

            grade.Identifier = dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(1).Value
            grade.FK_Subject = Ribbon.ActiveTab.Value
            grade.Date = DateTime.Now
            grade.FK_Term = GetActive.Term(Ribbon)

            Connection.GradesContext.Grade.AddObject(grade)
            Connection.GradesContext.SaveChanges()

            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(0).Value = grade.ID
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(1).Value = grade.Identifier
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(2).Value = grade.Grade1


            ' dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(3).Value = grade.Weight
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(3).Value = 1
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(4).Value = grade.Date
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(5).Value = grade.Comment

        ElseIf Not dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then

            Dim RecordID As Decimal = CDec(dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(0).Value)

            Dim grade = (From _grade In Connection.GradesContext.Grade Where _grade.ID = RecordID).Single()

            grade.Identifier = dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(1).Value
            grade.Grade1 = CDec(dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(2).Value)
            grade.Weight = Convert.ToDouble(dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(3).Value)
            grade.Comment = dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(5).Value
            grade.FK_Subject = Ribbon.ActiveTab.Value
            grade.Date = dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(4).Value
            grade.FK_Term = GetActive.Term(Ribbon)

            Connection.GradesContext.SaveChanges()

            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(0).Value = grade.ID
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(1).Value = grade.Identifier
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(2).Value = grade.Grade1
            ' dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(3).Value = grade.Weight
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(3).Value = 1
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(4).Value = grade.Date
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(5).Value = grade.Comment

        ElseIf _
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing And
            dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then
            Me.BeginInvoke(New MethodInvoker(AddressOf Data_GradesGrid_DeleteEmptyRow))
        End If
    End Sub


    Private Sub Data_GradesGrid_DeleteEmptyRow()
        If EditingCell < dgv_GradesGrid.Rows.Count Then
            dgv_GradesGrid.Rows.Remove(dgv_GradesGrid.Rows(EditingCell))
        End If
    End Sub

    Private Sub Data_GradesGrid_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) _
        Handles dgv_GradesGrid.EditingControlShowing

        Dim tb As TextBox = TryCast(e.Control, TextBox)
        If (tb IsNot Nothing) Then

            RemoveHandler tb.KeyPress, AddressOf Data_GradesGrid_Weight_KeyPress
            RemoveHandler tb.KeyPress, AddressOf Data_GradesGrid_Grade_KeyPress

            Select Case Me.dgv_GradesGrid.CurrentCell.ColumnIndex
                Case 2
                    AddHandler tb.KeyPress, AddressOf Data_GradesGrid_Grade_KeyPress
                Case 3
                    AddHandler tb.KeyPress, AddressOf Data_GradesGrid_Weight_KeyPress
            End Select

        End If
    End Sub


    Private Sub Data_GradesGrid_Weight_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)

        Select Case e.KeyChar.ToString()
            Case "1", "2", "3"
            Case ControlChars.Back
            Case Else
                e.Handled = True
        End Select
    End Sub

    Private Sub Data_GradesGrid_Grade_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)


        Select Case e.KeyChar.ToString()
            Case "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
                Dim dbl As Double = Convert.ToDouble(sender.Text & e.KeyChar.ToString())

                If dbl > 6.0 Or dbl < 1 Then
                    e.Handled = True
                End If

            Case "."
                If _
                    (sender.Text & e.KeyChar.ToString()).Split(".").Length = 3 Or
                    (sender.Text & e.KeyChar.ToString()) = "." Then
                    e.Handled = True
                End If
            Case (ControlChars.Back)
            Case Else
                e.Handled = True
        End Select
    End Sub
End Class

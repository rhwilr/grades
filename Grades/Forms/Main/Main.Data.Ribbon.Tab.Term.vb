﻿Imports Grades.DB.Repository

Partial Public Class Main
    Private Sub Data_Ribbon_Tab_Term_TermItems(rc_term_term As RibbonComboBox, School_ID As Integer)

        For Each _Term In _
            (From term In Connection.GradesContext.Term Where term.FK_School = School_ID).OrderBy(
                Function(term) term.Identifier)

            Dim rb_term_term_item As New RibbonButton()

            rb_term_term_item.Text = _Term.Identifier
            rb_term_term_item.Tag = "rc_term_term_item_:" & School_ID
            rb_term_term_item.Value = _Term.ID
            rb_term_term_item.Style = RibbonButtonStyle.DropDownListItem
            rb_term_term_item.CheckOnClick = true

            rc_term_term.DropDownItems.Add(rb_term_term_item)

            If Date.Now >= _Term.Date_Start And Date.Now <= _Term.Date_End Then
                rc_term_term.SelectedItem = rb_term_term_item
                rb_term_term_item.Checked = True
            End If

        Next
    End Sub
End Class

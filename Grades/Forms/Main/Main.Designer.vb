﻿
Imports Grades.Resources.DataGridViewControls

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Dim DataGridViewCellStyle28 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle29 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle30 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Ribbon = New System.Windows.Forms.Ribbon()
        Me.rb_orb_menu_School_Manager = New System.Windows.Forms.RibbonButton()
        Me.rb_orb_options_Settings = New System.Windows.Forms.RibbonOrbOptionButton()
        Me.rb_orb_options_About = New System.Windows.Forms.RibbonOrbOptionButton()
        Me.rt_TabDemo_demo = New System.Windows.Forms.RibbonTab()
        Me.rp_new_demo = New System.Windows.Forms.RibbonPanel()
        Me.rb_grade_add_demo = New System.Windows.Forms.RibbonButton()
        Me.rb_grade_edit_demo = New System.Windows.Forms.RibbonButton()
        Me.rb_grade_delete_demo = New System.Windows.Forms.RibbonButton()
        Me.dgv_GradesGrid = New System.Windows.Forms.DataGridView()
        Me.dgc_grades_GradesID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgc_grades_exam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgc_grades_grade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgc_grades_weight = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgc_grades_Date = New CalendarColumn()
        Me.dgc_grades_comment = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgv_GradesGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Ribbon
        '
        Me.Ribbon.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.Ribbon.Location = New System.Drawing.Point(0, 0)
        Me.Ribbon.Minimized = False
        Me.Ribbon.Name = "Ribbon"
        '
        '
        '
        Me.Ribbon.OrbDropDown.BorderRoundness = 8
        Me.Ribbon.OrbDropDown.Location = New System.Drawing.Point(0, 0)
        Me.Ribbon.OrbDropDown.MenuItems.Add(Me.rb_orb_menu_School_Manager)
        Me.Ribbon.OrbDropDown.MinimumSize = New System.Drawing.Size(0, 200)
        Me.Ribbon.OrbDropDown.Name = ""
        Me.Ribbon.OrbDropDown.OptionItems.Add(Me.rb_orb_options_Settings)
        Me.Ribbon.OrbDropDown.OptionItems.Add(Me.rb_orb_options_About)
        Me.Ribbon.OrbDropDown.RecentItemsCaption = "Schools"
        Me.Ribbon.OrbDropDown.Size = New System.Drawing.Size(527, 200)
        Me.Ribbon.OrbDropDown.TabIndex = 0
        Me.Ribbon.OrbImage = Nothing
        Me.Ribbon.OrbStyle = System.Windows.Forms.RibbonOrbStyle.Office_2010
        Me.Ribbon.OrbText = "File"
        '
        '
        '
        Me.Ribbon.QuickAcessToolbar.Visible = False
        Me.Ribbon.Size = New System.Drawing.Size(812, 130)
        Me.Ribbon.TabIndex = 0
        Me.Ribbon.Tabs.Add(Me.rt_TabDemo_demo)
        Me.Ribbon.TabsMargin = New System.Windows.Forms.Padding(12, 26, 20, 0)
        Me.Ribbon.Text = "Grades"
        '
        'rb_orb_menu_School_Manager
        '
        Me.rb_orb_menu_School_Manager.Image = CType(resources.GetObject("rb_orb_menu_School_Manager.Image"), System.Drawing.Image)
        Me.rb_orb_menu_School_Manager.SmallImage = CType(resources.GetObject("rb_orb_menu_School_Manager.SmallImage"), System.Drawing.Image)
        Me.rb_orb_menu_School_Manager.Text = "School Manager"
        '
        'rb_orb_options_Settings
        '
        Me.rb_orb_options_Settings.Image = CType(resources.GetObject("rb_orb_options_Settings.Image"), System.Drawing.Image)
        Me.rb_orb_options_Settings.SmallImage = CType(resources.GetObject("rb_orb_options_Settings.SmallImage"), System.Drawing.Image)
        Me.rb_orb_options_Settings.Text = "Settings"
        '
        'rb_orb_options_About
        '
        Me.rb_orb_options_About.Image = CType(resources.GetObject("rb_orb_options_About.Image"), System.Drawing.Image)
        Me.rb_orb_options_About.SmallImage = CType(resources.GetObject("rb_orb_options_About.SmallImage"), System.Drawing.Image)
        Me.rb_orb_options_About.Text = "About"
        '
        'rt_TabDemo_demo
        '
        Me.rt_TabDemo_demo.Panels.Add(Me.rp_new_demo)
        Me.rt_TabDemo_demo.Text = "Demo"
        '
        'rp_new_demo
        '
        Me.rp_new_demo.ButtonMoreEnabled = False
        Me.rp_new_demo.ButtonMoreVisible = False
        Me.rp_new_demo.Items.Add(Me.rb_grade_add_demo)
        Me.rp_new_demo.Items.Add(Me.rb_grade_edit_demo)
        Me.rp_new_demo.Items.Add(Me.rb_grade_delete_demo)
        Me.rp_new_demo.Text = "Grades"
        '
        'rb_grade_add_demo
        '
        Me.rb_grade_add_demo.Image = CType(resources.GetObject("rb_grade_add_demo.Image"), System.Drawing.Image)
        Me.rb_grade_add_demo.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Large
        Me.rb_grade_add_demo.SmallImage = CType(resources.GetObject("rb_grade_add_demo.SmallImage"), System.Drawing.Image)
        Me.rb_grade_add_demo.Text = "Add"
        '
        'rb_grade_edit_demo
        '
        Me.rb_grade_edit_demo.Image = CType(resources.GetObject("rb_grade_edit_demo.Image"), System.Drawing.Image)
        Me.rb_grade_edit_demo.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium
        Me.rb_grade_edit_demo.MinSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium
        Me.rb_grade_edit_demo.SmallImage = CType(resources.GetObject("rb_grade_edit_demo.SmallImage"), System.Drawing.Image)
        Me.rb_grade_edit_demo.Text = "Edit"
        '
        'rb_grade_delete_demo
        '
        Me.rb_grade_delete_demo.Image = CType(resources.GetObject("rb_grade_delete_demo.Image"), System.Drawing.Image)
        Me.rb_grade_delete_demo.MaxSizeMode = System.Windows.Forms.RibbonElementSizeMode.Medium
        Me.rb_grade_delete_demo.SmallImage = CType(resources.GetObject("rb_grade_delete_demo.SmallImage"), System.Drawing.Image)
        Me.rb_grade_delete_demo.Text = "Delete"
        '
        'dgv_GradesGrid
        '
        Me.dgv_GradesGrid.AllowUserToAddRows = False
        Me.dgv_GradesGrid.AllowUserToDeleteRows = False
        Me.dgv_GradesGrid.AllowUserToResizeRows = False
        Me.dgv_GradesGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_GradesGrid.BackgroundColor = System.Drawing.Color.White
        Me.dgv_GradesGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgv_GradesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_GradesGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgc_grades_GradesID, Me.dgc_grades_exam, Me.dgc_grades_grade, Me.dgc_grades_weight, Me.dgc_grades_Date, Me.dgc_grades_comment})
        Me.dgv_GradesGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgv_GradesGrid.GridColor = System.Drawing.Color.Gainsboro
        Me.dgv_GradesGrid.Location = New System.Drawing.Point(0, 130)
        Me.dgv_GradesGrid.MultiSelect = False
        Me.dgv_GradesGrid.Name = "dgv_GradesGrid"
        Me.dgv_GradesGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgv_GradesGrid.Size = New System.Drawing.Size(812, 411)
        Me.dgv_GradesGrid.TabIndex = 1
        '
        'dgc_grades_GradesID
        '
        Me.dgc_grades_GradesID.HeaderText = "Grades ID"
        Me.dgc_grades_GradesID.Name = "dgc_grades_GradesID"
        Me.dgc_grades_GradesID.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgc_grades_GradesID.Visible = False
        '
        'dgc_grades_exam
        '
        Me.dgc_grades_exam.FillWeight = 99.49239!
        Me.dgc_grades_exam.HeaderText = "Exam"
        Me.dgc_grades_exam.Name = "dgc_grades_exam"
        '
        'dgc_grades_grade
        '
        DataGridViewCellStyle28.Format = "N2"
        DataGridViewCellStyle28.NullValue = Nothing
        Me.dgc_grades_grade.DefaultCellStyle = DataGridViewCellStyle28
        Me.dgc_grades_grade.FillWeight = 99.49239!
        Me.dgc_grades_grade.HeaderText = "Grade"
        Me.dgc_grades_grade.Name = "dgc_grades_grade"
        '
        'dgc_grades_weight
        '
        DataGridViewCellStyle29.Format = "N2"
        DataGridViewCellStyle29.NullValue = Nothing
        Me.dgc_grades_weight.DefaultCellStyle = DataGridViewCellStyle29
        Me.dgc_grades_weight.FillWeight = 101.5228!
        Me.dgc_grades_weight.HeaderText = "Weight"
        Me.dgc_grades_weight.Name = "dgc_grades_weight"
        Me.dgc_grades_weight.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'dgc_grades_Date
        '
        DataGridViewCellStyle30.Format = "d"
        Me.dgc_grades_Date.DefaultCellStyle = DataGridViewCellStyle30
        Me.dgc_grades_Date.HeaderText = "Date"
        Me.dgc_grades_Date.Name = "dgc_grades_Date"
        Me.dgc_grades_Date.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgc_grades_Date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgc_grades_comment
        '
        Me.dgc_grades_comment.FillWeight = 99.49239!
        Me.dgc_grades_comment.HeaderText = "Comment"
        Me.dgc_grades_comment.Name = "dgc_grades_comment"
        '
        'Main
        '
        Me.ClientSize = New System.Drawing.Size(812, 541)
        Me.Controls.Add(Me.dgv_GradesGrid)
        Me.Controls.Add(Me.Ribbon)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(481, 359)
        Me.Name = "Main"
        Me.Text = "Grades"
        CType(Me.dgv_GradesGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Ribbon As System.Windows.Forms.Ribbon
    Friend WithEvents rb_orb_menu_School_Manager As System.Windows.Forms.RibbonButton
    Friend WithEvents rt_TabDemo_demo As System.Windows.Forms.RibbonTab
    Friend WithEvents rp_new_demo As System.Windows.Forms.RibbonPanel
    Friend WithEvents rb_grade_add_demo As System.Windows.Forms.RibbonButton
    Friend WithEvents rb_grade_edit_demo As System.Windows.Forms.RibbonButton
    Friend WithEvents rb_grade_delete_demo As System.Windows.Forms.RibbonButton
    Friend WithEvents rb_orb_options_Settings As System.Windows.Forms.RibbonOrbOptionButton
    Friend WithEvents dgv_GradesGrid As System.Windows.Forms.DataGridView
    Friend WithEvents dgc_grades_GradesID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_grades_exam As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_grades_grade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_grades_weight As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_grades_Date As CalendarColumn
    Friend WithEvents dgc_grades_comment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rb_orb_options_About As System.Windows.Forms.RibbonOrbOptionButton

End Class

﻿Imports Grades.Dialogbox
Imports Grades.Localization.Messages
Imports Grades.Localization
Imports Grades.DB.Repository

Partial Public Class Main
    Private Sub Event_Ribbon_Tab_ActiveChanged(sender As Object, e As EventArgs)
        Dim SelectedComboBoxItem As RibbonComboBox =
                Functions.Controls.FindRibbonComboBox("rc_term_term_:" & sender.Value, Ribbon).FirstOrDefault

        If (Not IsNothing(SelectedComboBoxItem)) Then
            Data_GradesGrid_SubjectGrades(sender.Value, SelectedComboBoxItem.SelectedValue)
        Else
            Data_GradesGrid_SubjectGrades(sender.Value)
        End If
    End Sub


    Private Sub Event_Ribbon_Tab_Grades_RibbonButtonAdd_Click(sender As Object, e As EventArgs)
        dgv_GradesGrid.Rows.Add()
        dgv_GradesGrid.ClearSelection()
        dgv_GradesGrid.CurrentCell = dgv_GradesGrid.Rows(dgv_GradesGrid.Rows.Count - 1).Cells(1)
        dgv_GradesGrid.Rows(dgv_GradesGrid.Rows.Count - 1).Cells(1).Selected = True
        dgv_GradesGrid.BeginEdit(True)
    End Sub


    Private Sub Event_Ribbon_Tab_Grades_RibbonButtonEdit_Click(sender As Object, e As EventArgs)

        If dgv_GradesGrid.CurrentCell.ColumnIndex = 0 Then
            dgv_GradesGrid.CurrentCell = dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(1)
        Else
            dgv_GradesGrid.CurrentCell =
                dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(dgv_GradesGrid.CurrentCell.ColumnIndex)
        End If
        dgv_GradesGrid.BeginEdit(True)
    End Sub

    Private Sub Event_Ribbon_Tab_Grades_RibbonButtonDelete_Click(sender As Object, e As EventArgs)

        If Not dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing Then

            Dim _
                message As _
                    New MessageDialog(
                        String.Format(Question.Delete_noundo_tp, GeneralUI.Grade,
                                      dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(1).Value),
                        MessageDialog.IconType.Question, MessageDialog.ButtonType.YesNo, True)


            If message.ShowDialog() = DialogResult.Yes Then

                Dim RecordID As Decimal = CDec(dgv_GradesGrid.Rows(dgv_GradesGrid.CurrentCell.RowIndex).Cells(0).Value)

                Dim grade = (From _grade In Connection.GradesContext.Grade Where _grade.ID = RecordID).Single()


                Connection.GradesContext.DeleteObject(grade)
                Connection.GradesContext.SaveChanges()

                Data_GradesGrid_SubjectGrade_Refresh()

            End If


        Else
            Data_GradesGrid_SubjectGrades(Ribbon.ActiveTab.Value)
        End If
    End Sub


    Private Sub Event_Ribbon_Tab_Term_RibonComboBoxTextBoxTextChanged(sender As Object, e As EventArgs)

        Data_GradesGrid_SubjectGrades(Ribbon.ActiveTab.Value, sender.SelectedValue)
    End Sub
End Class

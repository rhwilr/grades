﻿Imports Grades.DB.Repository

Partial Public Class Subject
    Private Sub Event_Button_OK_Click(sender As Object, e As EventArgs) Handles btn_ok.Click

        For i As Integer = 0 To dgv_Subjects.RowCount - 1
            If Not dgv_Subjects.Rows(i).IsNewRow Then
                Dim subject As New DB.Model.Subject

                subject.Identifier = dgv_Subjects.Rows(i).Cells(0).Value
                subject.FK_School = SchoolID

                Connection.GradesContext.Subject.AddObject(subject)
                Connection.GradesContext.SaveChanges()
            End If
        Next
        Me.Close()
    End Sub


    Private Sub Event_Button_Cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click
        Me.Close()
    End Sub
End Class

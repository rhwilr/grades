﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class School
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btn_cancel = New System.Windows.Forms.Button()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.lbl_school_identifier = New System.Windows.Forms.Label()
        Me.tb_school_identifier = New System.Windows.Forms.TextBox()
        Me.lbl_schhol_schoolday = New System.Windows.Forms.Label()
        Me.dgv_school_schoolday = New System.Windows.Forms.DataGridView()
        Me.dgc_Schoolday = New System.Windows.Forms.DataGridViewComboBoxColumn()
        CType(Me.dgv_school_schoolday, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btn_cancel
        '
        Me.btn_cancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_cancel.Location = New System.Drawing.Point(211, 300)
        Me.btn_cancel.Name = "btn_cancel"
        Me.btn_cancel.Size = New System.Drawing.Size(75, 23)
        Me.btn_cancel.TabIndex = 0
        Me.btn_cancel.Text = "Cancel"
        Me.btn_cancel.UseVisualStyleBackColor = True
        '
        'btn_ok
        '
        Me.btn_ok.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_ok.Location = New System.Drawing.Point(130, 300)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(75, 23)
        Me.btn_ok.TabIndex = 1
        Me.btn_ok.Text = "OK"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'lbl_school_identifier
        '
        Me.lbl_school_identifier.AutoSize = True
        Me.lbl_school_identifier.Location = New System.Drawing.Point(12, 9)
        Me.lbl_school_identifier.Name = "lbl_school_identifier"
        Me.lbl_school_identifier.Size = New System.Drawing.Size(149, 13)
        Me.lbl_school_identifier.TabIndex = 2
        Me.lbl_school_identifier.Text = "Type the Name of the School:"
        '
        'tb_school_identifier
        '
        Me.tb_school_identifier.Location = New System.Drawing.Point(15, 25)
        Me.tb_school_identifier.Name = "tb_school_identifier"
        Me.tb_school_identifier.Size = New System.Drawing.Size(271, 20)
        Me.tb_school_identifier.TabIndex = 3
        '
        'lbl_schhol_schoolday
        '
        Me.lbl_schhol_schoolday.AutoSize = True
        Me.lbl_schhol_schoolday.Location = New System.Drawing.Point(12, 60)
        Me.lbl_schhol_schoolday.Name = "lbl_schhol_schoolday"
        Me.lbl_schhol_schoolday.Size = New System.Drawing.Size(57, 13)
        Me.lbl_schhol_schoolday.TabIndex = 4
        Me.lbl_schhol_schoolday.Text = "Schoolday"
        '
        'dgv_school_schoolday
        '
        Me.dgv_school_schoolday.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_school_schoolday.BackgroundColor = System.Drawing.Color.White
        Me.dgv_school_schoolday.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_school_schoolday.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgc_Schoolday})
        Me.dgv_school_schoolday.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgv_school_schoolday.Location = New System.Drawing.Point(15, 76)
        Me.dgv_school_schoolday.Name = "dgv_school_schoolday"
        Me.dgv_school_schoolday.RowHeadersVisible = False
        Me.dgv_school_schoolday.Size = New System.Drawing.Size(271, 218)
        Me.dgv_school_schoolday.TabIndex = 5
        '
        'dgc_Schoolday
        '
        Me.dgc_Schoolday.HeaderText = "Schoolday"
        Me.dgc_Schoolday.Items.AddRange(New Object() {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"})
        Me.dgc_Schoolday.Name = "dgc_Schoolday"
        '
        'School
        '
        Me.AcceptButton = Me.btn_ok
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btn_cancel
        Me.ClientSize = New System.Drawing.Size(298, 335)
        Me.Controls.Add(Me.dgv_school_schoolday)
        Me.Controls.Add(Me.lbl_schhol_schoolday)
        Me.Controls.Add(Me.tb_school_identifier)
        Me.Controls.Add(Me.lbl_school_identifier)
        Me.Controls.Add(Me.btn_ok)
        Me.Controls.Add(Me.btn_cancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "School"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "School"
        CType(Me.dgv_school_schoolday, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btn_cancel As System.Windows.Forms.Button
    Friend WithEvents btn_ok As System.Windows.Forms.Button
    Friend WithEvents lbl_school_identifier As System.Windows.Forms.Label
    Friend WithEvents tb_school_identifier As System.Windows.Forms.TextBox
    Friend WithEvents lbl_schhol_schoolday As System.Windows.Forms.Label
    Friend WithEvents dgv_school_schoolday As System.Windows.Forms.DataGridView
    Friend WithEvents dgc_Schoolday As System.Windows.Forms.DataGridViewComboBoxColumn
End Class

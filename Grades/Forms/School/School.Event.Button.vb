﻿Imports Grades.DB.Model
Imports Grades.DB.Repository

Partial Public Class School
    Private Sub Ëvent_Button_Ok_Click(sender As Object, e As EventArgs) Handles btn_ok.Click

        Dim school As New DB.Model.School
        Dim schoolday As New Schoolday

        school.Identifier = tb_school_identifier.Text
        schoolday.Schoolday1 = 1

        schoolday.School = school

        Connection.GradesContext.School.AddObject(school)
        Connection.GradesContext.Schoolday.AddObject(schoolday)
        Connection.GradesContext.SaveChanges()

        Main.Data_Ribbon_Orb_SchoolRecent()

        Me.Close()

        Dim Subject As New Subject
        Subject.SchoolID = school.ID
        Subject.ShowDialog()
    End Sub

    Private Sub Ëvent_Button_Cancel_Click(sender As Object, e As EventArgs) Handles btn_cancel.Click
        Me.Close()
    End Sub
End Class

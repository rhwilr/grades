﻿Imports Grades.DB.Model
Imports Grades.DB.Repository

Partial Public Class School_Manager
    Private Sub Event_SchooldaytGrid_EditingControlShowing(sender As Object,
                                                           e As DataGridViewEditingControlShowingEventArgs) _
        Handles dgv_SchooldayGrid.EditingControlShowing
        If dgv_SchooldayGrid.CurrentCell.ColumnIndex = 1 Then
            Dim comboBox As ComboBox = TryCast(e.Control, ComboBox)
            RemoveHandler comboBox.SelectedIndexChanged, AddressOf Event_SchooldaytGrid_SelectedIndexChanged
            AddHandler comboBox.SelectedIndexChanged, AddressOf Event_SchooldaytGrid_SelectedIndexChanged
        End If
    End Sub


    Private Sub Event_SchooldaytGrid_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim selectedIndex As Integer = DirectCast(sender, ComboBox).SelectedIndex

        dgv_SchooldayGrid.EndEdit(True)

        If dgv_SchooldayGrid.Rows(dgv_SchooldayGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing Then

            Dim schoolday As New Schoolday

            schoolday.Schoolday1 = selectedIndex
            schoolday.FK_School = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value

            Connection.GradesContext.Schoolday.AddObject(schoolday)

            Connection.GradesContext.SaveChanges()

            dgv_SchooldayGrid.Rows(dgv_SchooldayGrid.CurrentCell.RowIndex).Cells(0).Value = schoolday.ID

        Else

            Dim RecordID As Decimal = CDec(dgv_SchooldayGrid.Rows(dgv_SchooldayGrid.CurrentCell.RowIndex).Cells(0).Value)

            Dim schoolday =
                    (From _schoolday In Connection.GradesContext.Schoolday Where _schoolday.ID = RecordID).Single()

            schoolday.Schoolday1 = selectedIndex

            Connection.GradesContext.SaveChanges()
        End If
    End Sub
End Class

﻿Imports Grades.Dialogbox
Imports Grades.Localization.Messages
Imports Grades.Localization
Imports Grades.DB.Repository

Public Class School_Manager
    Private Sub Event_Button_SchoolGrid_add_Click(sender As Object, e As EventArgs) _
        Handles btn_SchoolGrid_add.Click

        dgv_SchoolGrid.Rows.Add()
        dgv_SchoolGrid.ClearSelection()
        dgv_SchoolGrid.CurrentCell = dgv_SchoolGrid.Rows(dgv_SchoolGrid.Rows.Count - 1).Cells(1)
        dgv_SchoolGrid.Rows(dgv_SchoolGrid.Rows.Count - 1).Cells(1).Selected = True
        dgv_SchoolGrid.BeginEdit(True)
    End Sub


    Private Sub Event_Button_SchooldayGrid_add_Click(sender As Object, e As EventArgs) _
        Handles btn_SchooldayGrid_add.Click

        dgv_SchooldayGrid.Rows.Add()
        dgv_SchooldayGrid.ClearSelection()
        dgv_SchooldayGrid.CurrentCell = dgv_SchooldayGrid.Rows(dgv_SchooldayGrid.Rows.Count - 1).Cells(1)
        dgv_SchooldayGrid.Rows(dgv_SchooldayGrid.Rows.Count - 1).Cells(1).Selected = True
        dgv_SchooldayGrid.BeginEdit(True)
    End Sub

    Private Sub Event_Button_SubjectGrid_add_Click(sender As Object, e As EventArgs) Handles btn_SubjectGrid_add.Click

        dgv_SubjectGrid.Rows.Add()
        dgv_SubjectGrid.ClearSelection()
        dgv_SubjectGrid.CurrentCell = dgv_SubjectGrid.Rows(dgv_SubjectGrid.Rows.Count - 1).Cells(1)
        dgv_SubjectGrid.Rows(dgv_SubjectGrid.Rows.Count - 1).Cells(1).Selected = True
        dgv_SubjectGrid.BeginEdit(True)
    End Sub

    Private Sub Event_Button_TermGrid_add_Click(sender As Object, e As EventArgs) Handles btn_TermGrid_add.Click

        dgv_termGrid.Rows.Add()
        dgv_termGrid.ClearSelection()
        dgv_termGrid.CurrentCell = dgv_termGrid.Rows(dgv_termGrid.Rows.Count - 1).Cells(1)
        dgv_termGrid.Rows(dgv_termGrid.Rows.Count - 1).Cells(1).Selected = True
        dgv_termGrid.BeginEdit(True)
    End Sub


    Private Sub btn_SchoolGrid_delete_Click(sender As Object, e As EventArgs) Handles btn_SchoolGrid_delete.Click
        Dim _
            message As _
                New MessageDialog(
                    String.Format(Question.Delete_noundo_tp, GeneralUI.School,
                                  dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(1).Value),
                    MessageDialog.IconType.Question, MessageDialog.ButtonType.YesNo, True)

        If Not dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing Then

            If message.ShowDialog() = DialogResult.Yes Then

                Dim RecordID As Integer = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value

                Dim School =
                        (From _School In Connection.GradesContext.School Where _School.ID = RecordID).Single()


                Connection.GradesContext.DeleteObject(School)
                Connection.GradesContext.SaveChanges()

                Data_SchoolGrid_School()

            End If
        Else
            Data_SchoolGrid_School()
        End If
    End Sub


    Private Sub btn_SchooldayGrid_delete_Click(sender As Object, e As EventArgs) Handles btn_SchooldayGrid_delete.Click


        Dim _
            message As _
                New MessageDialog(
                    String.Format(Question.Delete_noundo_tp, GeneralUI.Schoolday,
                                  dgv_SchooldayGrid.Rows(dgv_SchooldayGrid.CurrentCell.RowIndex).Cells(1).Value),
                    MessageDialog.IconType.Question, MessageDialog.ButtonType.YesNo, True)

        If Not dgv_SchooldayGrid.Rows(dgv_SchooldayGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing Then
            If message.ShowDialog() = DialogResult.Yes Then

                Dim RecordID As Integer = dgv_SchooldayGrid.Rows(dgv_SchooldayGrid.CurrentCell.RowIndex).Cells(0).Value

                Dim schoolday =
                        (From _schoolday In Connection.GradesContext.Schoolday Where _schoolday.ID = RecordID).Single()


                Connection.GradesContext.DeleteObject(schoolday)
                Connection.GradesContext.SaveChanges()


                Dim SchoolID As Integer = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value
                If Not SchoolID = Nothing Then
                    Data_SchooldayGrid_Schoolday(SchoolID)
                End If

            End If
        Else
            Dim SchoolID As Integer = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value
            If Not SchoolID = Nothing Then
                Data_SchooldayGrid_Schoolday(SchoolID)
            End If
        End If
    End Sub

    Private Sub btn_SubjectGrid_delete_Click(sender As Object, e As EventArgs) Handles btn_SubjectGrid_delete.Click

        Dim _
            message As _
                New MessageDialog(
                    String.Format(Question.Delete_noundo_tp, GeneralUI.Subject,
                                  dgv_SubjectGrid.Rows(dgv_SubjectGrid.CurrentCell.RowIndex).Cells(1).Value),
                    MessageDialog.IconType.Question, MessageDialog.ButtonType.YesNo, True)

        If Not dgv_SubjectGrid.Rows(dgv_SubjectGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing Then

            If message.ShowDialog() = DialogResult.Yes Then


                Dim RecordID As Integer = dgv_SubjectGrid.Rows(dgv_SubjectGrid.CurrentCell.RowIndex).Cells(0).Value

                Dim Subject = (From _Subject In Connection.GradesContext.Subject Where _Subject.ID = RecordID).Single()


                Connection.GradesContext.DeleteObject(Subject)
                Connection.GradesContext.SaveChanges()


                Dim SchoolID As Integer = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value
                If Not SchoolID = Nothing Then
                    Data_SubjectGrid_Subject(SchoolID)
                End If

            End If

        Else
            Dim SchoolID As Integer = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value
            If Not SchoolID = Nothing Then
                Data_SubjectGrid_Subject(SchoolID)
            End If
        End If
    End Sub

    Private Sub btn_TermGrid_delete_Click(sender As Object, e As EventArgs) Handles btn_TermGrid_delete.Click

        Dim _
            message As _
                New MessageDialog(
                    String.Format(Question.Delete_noundo_tp, GeneralUI.Term,
                                  dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(1).Value),
                    MessageDialog.IconType.Question, MessageDialog.ButtonType.YesNo, True)

        If Not dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing Then
            If message.ShowDialog() = DialogResult.Yes Then


                Dim RecordID As Integer = dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(0).Value

                Dim Term = (From _Term In Connection.GradesContext.Term Where _Term.ID = RecordID).Single()


                Connection.GradesContext.DeleteObject(Term)
                Connection.GradesContext.SaveChanges()


                Dim SchoolID As Integer = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value
                If Not SchoolID = Nothing Then
                    Data_TermGrid_Term(SchoolID)
                End If

            End If
        Else
            Dim SchoolID As Integer = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value
            If Not SchoolID = Nothing Then
                Data_TermGrid_Term(SchoolID)
            End If
        End If
    End Sub
End Class

﻿Imports Grades.DB.Repository

Partial Public Class School_Manager
    Private Sub Data_SchoolGrid_School()

        dgv_SchoolGrid.Rows.Clear()

        For Each _School In (From school In Connection.GradesContext.School).OrderBy(Function(school) school.Identifier)
            dgv_SchoolGrid.Rows.Add(_School.ID, _School.Identifier)
        Next
    End Sub
End Class

﻿Imports Grades.DB.Repository

Partial Public Class School_Manager
    Private Sub Data_TermGrid_Term(SchoolID As Integer)

        dgv_termGrid.Rows.Clear()

        For Each _Term In _
            (From term In Connection.GradesContext.Term Where term.FK_School = SchoolID).OrderBy(
                Function(term) term.Identifier)
            dgv_termGrid.Rows.Add(_Term.ID, _Term.Identifier, _Term.Date_Start, _Term.Date_End)
        Next
    End Sub
End Class

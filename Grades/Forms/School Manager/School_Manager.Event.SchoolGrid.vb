﻿Imports Grades.DB.Repository

Partial Public Class School_Manager
    Private EditingCell As Integer

    Private Sub Data_SchoolGrid_CellBeginEdit(sender As Object, e As DataGridViewCellCancelEventArgs) _
        Handles dgv_SchoolGrid.CellBeginEdit
        EditingCell = dgv_SchoolGrid.CurrentCell.RowIndex
    End Sub

    Private Sub Event_SchoolGrid_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_SchoolGrid.CellEndEdit

        If _
            dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing And
            Not dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then

            Dim school As New DB.Model.School

            school.Identifier = dgv_SchoolGrid.CurrentCell.Value

            Connection.GradesContext.School.AddObject(school)

            Connection.GradesContext.SaveChanges()

            dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value = school.ID

        ElseIf Not dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then

            Dim RecordID As Decimal = CDec(dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value)

            Dim school = (From _school In Connection.GradesContext.School Where _school.ID = RecordID).Single()

            school.Identifier = dgv_SchoolGrid.CurrentCell.Value

            Connection.GradesContext.SaveChanges()
        ElseIf _
            dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing And
            dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then
            BeginInvoke(New MethodInvoker(AddressOf Data_SchoolGrid_DeleteEmptyRow))
        End If
        Event_SchoolGrid_SelectionChanged()
    End Sub


    Private Sub Data_SchoolGrid_DeleteEmptyRow()
        If EditingCell < dgv_SchoolGrid.Rows.Count Then
            dgv_SchoolGrid.Rows.Remove(dgv_SchoolGrid.Rows(EditingCell))
        End If
    End Sub

    Private Sub Event_SchoolGrid_SelectionChanged() Handles dgv_SchoolGrid.SelectionChanged

        Dim SchoolID As Integer = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value

        lbl_Settings_Title.Text = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(1).Value

        If Not SchoolID = Nothing Then

            Data_SchooldayGrid_Schoolday(SchoolID)

            Data_SubjectGrid_Subject(SchoolID)

            Data_TermGrid_Term(SchoolID)

        End If
    End Sub
End Class
﻿Imports Grades.DB.Repository

Partial Public Class School_Manager
    Private Sub Event_SubjectGrid_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_SubjectGrid.CellEndEdit

        If _
            dgv_SubjectGrid.Rows(dgv_SubjectGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing And
            Not dgv_SubjectGrid.Rows(dgv_SubjectGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then

            Dim subject As New DB.Model.Subject

            subject.Identifier = dgv_SubjectGrid.CurrentCell.Value
            subject.FK_School = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value

            Connection.GradesContext.Subject.AddObject(subject)

            Connection.GradesContext.SaveChanges()


            dgv_SubjectGrid.Rows(dgv_SubjectGrid.CurrentCell.RowIndex).Cells(0).Value = subject.ID

        ElseIf Not dgv_SubjectGrid.Rows(dgv_SubjectGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then

            Dim RecordID As Decimal = CDec(dgv_SubjectGrid.Rows(dgv_SubjectGrid.CurrentCell.RowIndex).Cells(0).Value)

            Dim subject = (From _subject In Connection.GradesContext.Subject Where _subject.ID = RecordID).Single()

            subject.Identifier = dgv_SubjectGrid.CurrentCell.Value

            Connection.GradesContext.SaveChanges()
        End If
    End Sub
End Class

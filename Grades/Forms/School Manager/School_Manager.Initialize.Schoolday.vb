﻿Imports Grades.Localization

Partial Public Class School_Manager
    Private Sub Initialize_Schoolday_LoadDays()
        dgc_Schoolday.Items.Clear()
        dgc_Schoolday.Items.AddRange(Dates.Monday, Dates.Tuesday, Dates.Wednesday, Dates.Thursday, Dates.Friday,
                                     Dates.Saturday, Dates.Sunday)
    End Sub
End Class

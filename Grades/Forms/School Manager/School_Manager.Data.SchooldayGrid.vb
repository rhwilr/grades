﻿Imports Grades.DB.Repository

Partial Public Class School_Manager
    Private Sub Data_SchooldayGrid_Schoolday(SchoolID As Integer)

        dgv_SchooldayGrid.Rows.Clear()

        For Each _Schoolday In _
            (From schoolday In Connection.GradesContext.Schoolday Where schoolday.FK_School = SchoolID).OrderBy(
                Function(schoolday) schoolday.Schoolday1)
            dgv_SchooldayGrid.Rows.Add(_Schoolday.ID, dgc_Schoolday.Items(_Schoolday.Schoolday1))
        Next
    End Sub
End Class

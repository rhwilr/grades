﻿Imports Grades.Localization

Partial Public Class School_Manager
    Private Sub Localize_School_Manager()
        Me.Text = GeneralUI.School_Manager

        lbl_school_manager_schoolday_title.Text = GeneralUI.Schoolday
        lbl_school_manager_schoolday_description.Text = Localization.Text.schoolmanager_schoolday_description

        lbl_school_manager_subject_title.Text = GeneralUI.Subject
        lbl_school_manager_subject_description.Text = Localization.Text.schoolmanager_subject_description

        lbl_school_manager_term_title.Text = GeneralUI.Term
        lbl_school_manager_term_description.Text = Localization.Text.schoolmanager_term_description
    End Sub
End Class

﻿Imports Grades.DB.Repository

Partial Public Class School_Manager
    Private Sub Data_SubjectGrid_Subject(SchoolID As Integer)

        dgv_SubjectGrid.Rows.Clear()

        For Each _Subject In _
            (From subject In Connection.GradesContext.Subject Where subject.FK_School = SchoolID).OrderBy(
                Function(subject) subject.Identifier)
            dgv_SubjectGrid.Rows.Add(_Subject.ID, _Subject.Identifier)
        Next
    End Sub
End Class

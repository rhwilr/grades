﻿Imports Grades.Resources.DataGridViewControls

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class School_Manager
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgv_SchoolGrid = New System.Windows.Forms.DataGridView()
        Me.dgc_school_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgc_school_school = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn_ok = New System.Windows.Forms.Button()
        Me.dgv_SubjectGrid = New System.Windows.Forms.DataGridView()
        Me.dgc_subject_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgc_subject_subject = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgv_SchooldayGrid = New System.Windows.Forms.DataGridView()
        Me.dgc_Schoolday_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgc_Schoolday = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.pnl_Settings_Title = New System.Windows.Forms.Panel()
        Me.lbl_Settings_Title = New System.Windows.Forms.Label()
        Me.pnl_Settings_Controls = New System.Windows.Forms.Panel()
        Me.btn_TermGrid_delete = New System.Windows.Forms.Button()
        Me.btn_TermGrid_add = New System.Windows.Forms.Button()
        Me.btn_SubjectGrid_delete = New System.Windows.Forms.Button()
        Me.btn_SubjectGrid_add = New System.Windows.Forms.Button()
        Me.btn_SchooldayGrid_delete = New System.Windows.Forms.Button()
        Me.btn_SchooldayGrid_add = New System.Windows.Forms.Button()
        Me.lbl_school_manager_term_description = New System.Windows.Forms.Label()
        Me.lbl_school_manager_subject_description = New System.Windows.Forms.Label()
        Me.lbl_school_manager_term_title = New System.Windows.Forms.Label()
        Me.lbl_school_manager_subject_title = New System.Windows.Forms.Label()
        Me.lbl_school_manager_schoolday_description = New System.Windows.Forms.Label()
        Me.dgv_termGrid = New System.Windows.Forms.DataGridView()
        Me.dgc_term_ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgc_term_term = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgc_term_startdate = New CalendarColumn()
        Me.dgc_term_enddate = New CalendarColumn()
        Me.lbl_school_manager_schoolday_title = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CalendarColumn1 = New CalendarColumn()
        Me.CalendarColumn2 = New CalendarColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btn_SchoolGrid_delete = New System.Windows.Forms.Button()
        Me.btn_SchoolGrid_add = New System.Windows.Forms.Button()
        CType(Me.dgv_SchoolGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_SubjectGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv_SchooldayGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_Settings_Title.SuspendLayout()
        Me.pnl_Settings_Controls.SuspendLayout()
        CType(Me.dgv_termGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgv_SchoolGrid
        '
        Me.dgv_SchoolGrid.AllowUserToAddRows = False
        Me.dgv_SchoolGrid.AllowUserToDeleteRows = False
        Me.dgv_SchoolGrid.AllowUserToResizeColumns = False
        Me.dgv_SchoolGrid.AllowUserToResizeRows = False
        Me.dgv_SchoolGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgv_SchoolGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_SchoolGrid.BackgroundColor = System.Drawing.Color.White
        Me.dgv_SchoolGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_SchoolGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv_SchoolGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_SchoolGrid.ColumnHeadersVisible = False
        Me.dgv_SchoolGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgc_school_id, Me.dgc_school_school})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_SchoolGrid.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgv_SchoolGrid.Location = New System.Drawing.Point(12, 12)
        Me.dgv_SchoolGrid.MultiSelect = False
        Me.dgv_SchoolGrid.Name = "dgv_SchoolGrid"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_SchoolGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgv_SchoolGrid.RowHeadersVisible = False
        Me.dgv_SchoolGrid.RowTemplate.Height = 21
        Me.dgv_SchoolGrid.Size = New System.Drawing.Size(205, 480)
        Me.dgv_SchoolGrid.TabIndex = 0
        '
        'dgc_school_id
        '
        Me.dgc_school_id.HeaderText = "ID"
        Me.dgc_school_id.Name = "dgc_school_id"
        Me.dgc_school_id.Visible = False
        '
        'dgc_school_school
        '
        Me.dgc_school_school.HeaderText = "School"
        Me.dgc_school_school.Name = "dgc_school_school"
        '
        'btn_ok
        '
        Me.btn_ok.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_ok.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btn_ok.Location = New System.Drawing.Point(588, 498)
        Me.btn_ok.Name = "btn_ok"
        Me.btn_ok.Size = New System.Drawing.Size(75, 23)
        Me.btn_ok.TabIndex = 3
        Me.btn_ok.Text = "OK"
        Me.btn_ok.UseVisualStyleBackColor = True
        '
        'dgv_SubjectGrid
        '
        Me.dgv_SubjectGrid.AllowUserToAddRows = False
        Me.dgv_SubjectGrid.AllowUserToDeleteRows = False
        Me.dgv_SubjectGrid.AllowUserToResizeColumns = False
        Me.dgv_SubjectGrid.AllowUserToResizeRows = False
        Me.dgv_SubjectGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_SubjectGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_SubjectGrid.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_SubjectGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgv_SubjectGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_SubjectGrid.ColumnHeadersVisible = False
        Me.dgv_SubjectGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgc_subject_ID, Me.dgc_subject_subject})
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_SubjectGrid.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgv_SubjectGrid.Location = New System.Drawing.Point(9, 163)
        Me.dgv_SubjectGrid.MultiSelect = False
        Me.dgv_SubjectGrid.Name = "dgv_SubjectGrid"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_SubjectGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgv_SubjectGrid.RowHeadersWidth = 25
        Me.dgv_SubjectGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgv_SubjectGrid.Size = New System.Drawing.Size(369, 108)
        Me.dgv_SubjectGrid.TabIndex = 4
        '
        'dgc_subject_ID
        '
        Me.dgc_subject_ID.HeaderText = "ID"
        Me.dgc_subject_ID.Name = "dgc_subject_ID"
        Me.dgc_subject_ID.Visible = False
        '
        'dgc_subject_subject
        '
        Me.dgc_subject_subject.HeaderText = "Subject"
        Me.dgc_subject_subject.Name = "dgc_subject_subject"
        '
        'dgv_SchooldayGrid
        '
        Me.dgv_SchooldayGrid.AllowUserToAddRows = False
        Me.dgv_SchooldayGrid.AllowUserToDeleteRows = False
        Me.dgv_SchooldayGrid.AllowUserToResizeColumns = False
        Me.dgv_SchooldayGrid.AllowUserToResizeRows = False
        Me.dgv_SchooldayGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_SchooldayGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_SchooldayGrid.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_SchooldayGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgv_SchooldayGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_SchooldayGrid.ColumnHeadersVisible = False
        Me.dgv_SchooldayGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgc_Schoolday_ID, Me.dgc_Schoolday})
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_SchooldayGrid.DefaultCellStyle = DataGridViewCellStyle8
        Me.dgv_SchooldayGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgv_SchooldayGrid.Location = New System.Drawing.Point(9, 36)
        Me.dgv_SchooldayGrid.MultiSelect = False
        Me.dgv_SchooldayGrid.Name = "dgv_SchooldayGrid"
        Me.dgv_SchooldayGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_SchooldayGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.dgv_SchooldayGrid.RowHeadersWidth = 25
        Me.dgv_SchooldayGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgv_SchooldayGrid.Size = New System.Drawing.Size(369, 85)
        Me.dgv_SchooldayGrid.TabIndex = 6
        '
        'dgc_Schoolday_ID
        '
        Me.dgc_Schoolday_ID.HeaderText = "ID"
        Me.dgc_Schoolday_ID.Name = "dgc_Schoolday_ID"
        Me.dgc_Schoolday_ID.Visible = False
        '
        'dgc_Schoolday
        '
        Me.dgc_Schoolday.HeaderText = "Schoolday"
        Me.dgc_Schoolday.Name = "dgc_Schoolday"
        '
        'pnl_Settings_Title
        '
        Me.pnl_Settings_Title.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnl_Settings_Title.Controls.Add(Me.lbl_Settings_Title)
        Me.pnl_Settings_Title.Location = New System.Drawing.Point(223, 12)
        Me.pnl_Settings_Title.Name = "pnl_Settings_Title"
        Me.pnl_Settings_Title.Size = New System.Drawing.Size(436, 26)
        Me.pnl_Settings_Title.TabIndex = 9
        '
        'lbl_Settings_Title
        '
        Me.lbl_Settings_Title.AutoSize = True
        Me.lbl_Settings_Title.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Settings_Title.Location = New System.Drawing.Point(3, 0)
        Me.lbl_Settings_Title.Name = "lbl_Settings_Title"
        Me.lbl_Settings_Title.Size = New System.Drawing.Size(35, 18)
        Me.lbl_Settings_Title.TabIndex = 0
        Me.lbl_Settings_Title.Text = "Title"
        '
        'pnl_Settings_Controls
        '
        Me.pnl_Settings_Controls.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnl_Settings_Controls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_Settings_Controls.Controls.Add(Me.btn_TermGrid_delete)
        Me.pnl_Settings_Controls.Controls.Add(Me.btn_TermGrid_add)
        Me.pnl_Settings_Controls.Controls.Add(Me.btn_SubjectGrid_delete)
        Me.pnl_Settings_Controls.Controls.Add(Me.btn_SubjectGrid_add)
        Me.pnl_Settings_Controls.Controls.Add(Me.btn_SchooldayGrid_delete)
        Me.pnl_Settings_Controls.Controls.Add(Me.btn_SchooldayGrid_add)
        Me.pnl_Settings_Controls.Controls.Add(Me.lbl_school_manager_term_description)
        Me.pnl_Settings_Controls.Controls.Add(Me.lbl_school_manager_subject_description)
        Me.pnl_Settings_Controls.Controls.Add(Me.lbl_school_manager_term_title)
        Me.pnl_Settings_Controls.Controls.Add(Me.lbl_school_manager_subject_title)
        Me.pnl_Settings_Controls.Controls.Add(Me.lbl_school_manager_schoolday_description)
        Me.pnl_Settings_Controls.Controls.Add(Me.dgv_termGrid)
        Me.pnl_Settings_Controls.Controls.Add(Me.lbl_school_manager_schoolday_title)
        Me.pnl_Settings_Controls.Controls.Add(Me.dgv_SubjectGrid)
        Me.pnl_Settings_Controls.Controls.Add(Me.dgv_SchooldayGrid)
        Me.pnl_Settings_Controls.Location = New System.Drawing.Point(223, 44)
        Me.pnl_Settings_Controls.Name = "pnl_Settings_Controls"
        Me.pnl_Settings_Controls.Size = New System.Drawing.Size(437, 448)
        Me.pnl_Settings_Controls.TabIndex = 8
        '
        'btn_TermGrid_delete
        '
        Me.btn_TermGrid_delete.Location = New System.Drawing.Point(384, 344)
        Me.btn_TermGrid_delete.Name = "btn_TermGrid_delete"
        Me.btn_TermGrid_delete.Size = New System.Drawing.Size(30, 23)
        Me.btn_TermGrid_delete.TabIndex = 13
        Me.btn_TermGrid_delete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btn_TermGrid_delete.UseVisualStyleBackColor = True
        '
        'btn_TermGrid_add
        '
        Me.btn_TermGrid_add.Location = New System.Drawing.Point(384, 315)
        Me.btn_TermGrid_add.Name = "btn_TermGrid_add"
        Me.btn_TermGrid_add.Size = New System.Drawing.Size(30, 23)
        Me.btn_TermGrid_add.TabIndex = 14
        Me.btn_TermGrid_add.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btn_TermGrid_add.UseVisualStyleBackColor = True
        '
        'btn_SubjectGrid_delete
        '
        Me.btn_SubjectGrid_delete.Location = New System.Drawing.Point(384, 192)
        Me.btn_SubjectGrid_delete.Name = "btn_SubjectGrid_delete"
        Me.btn_SubjectGrid_delete.Size = New System.Drawing.Size(30, 23)
        Me.btn_SubjectGrid_delete.TabIndex = 11
        Me.btn_SubjectGrid_delete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btn_SubjectGrid_delete.UseVisualStyleBackColor = True
        '
        'btn_SubjectGrid_add
        '
        Me.btn_SubjectGrid_add.Location = New System.Drawing.Point(384, 163)
        Me.btn_SubjectGrid_add.Name = "btn_SubjectGrid_add"
        Me.btn_SubjectGrid_add.Size = New System.Drawing.Size(30, 23)
        Me.btn_SubjectGrid_add.TabIndex = 12
        Me.btn_SubjectGrid_add.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btn_SubjectGrid_add.UseVisualStyleBackColor = True
        '
        'btn_SchooldayGrid_delete
        '
        Me.btn_SchooldayGrid_delete.Location = New System.Drawing.Point(385, 65)
        Me.btn_SchooldayGrid_delete.Name = "btn_SchooldayGrid_delete"
        Me.btn_SchooldayGrid_delete.Size = New System.Drawing.Size(30, 23)
        Me.btn_SchooldayGrid_delete.TabIndex = 10
        Me.btn_SchooldayGrid_delete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btn_SchooldayGrid_delete.UseVisualStyleBackColor = True
        '
        'btn_SchooldayGrid_add
        '
        Me.btn_SchooldayGrid_add.Location = New System.Drawing.Point(385, 36)
        Me.btn_SchooldayGrid_add.Name = "btn_SchooldayGrid_add"
        Me.btn_SchooldayGrid_add.Size = New System.Drawing.Size(30, 23)
        Me.btn_SchooldayGrid_add.TabIndex = 10
        Me.btn_SchooldayGrid_add.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btn_SchooldayGrid_add.UseVisualStyleBackColor = True
        '
        'lbl_school_manager_term_description
        '
        Me.lbl_school_manager_term_description.AutoSize = True
        Me.lbl_school_manager_term_description.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_school_manager_term_description.Location = New System.Drawing.Point(6, 299)
        Me.lbl_school_manager_term_description.Name = "lbl_school_manager_term_description"
        Me.lbl_school_manager_term_description.Size = New System.Drawing.Size(140, 13)
        Me.lbl_school_manager_term_description.TabIndex = 8
        Me.lbl_school_manager_term_description.Text = "Add the terms of this school."
        '
        'lbl_school_manager_subject_description
        '
        Me.lbl_school_manager_subject_description.AutoSize = True
        Me.lbl_school_manager_subject_description.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_school_manager_subject_description.Location = New System.Drawing.Point(6, 147)
        Me.lbl_school_manager_subject_description.Name = "lbl_school_manager_subject_description"
        Me.lbl_school_manager_subject_description.Size = New System.Drawing.Size(234, 13)
        Me.lbl_school_manager_subject_description.TabIndex = 8
        Me.lbl_school_manager_subject_description.Text = "Add the Subjects which you have at this school."
        '
        'lbl_school_manager_term_title
        '
        Me.lbl_school_manager_term_title.AutoSize = True
        Me.lbl_school_manager_term_title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_school_manager_term_title.Location = New System.Drawing.Point(6, 283)
        Me.lbl_school_manager_term_title.Name = "lbl_school_manager_term_title"
        Me.lbl_school_manager_term_title.Size = New System.Drawing.Size(41, 13)
        Me.lbl_school_manager_term_title.TabIndex = 9
        Me.lbl_school_manager_term_title.Text = "Terms"
        '
        'lbl_school_manager_subject_title
        '
        Me.lbl_school_manager_subject_title.AutoSize = True
        Me.lbl_school_manager_subject_title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_school_manager_subject_title.Location = New System.Drawing.Point(6, 131)
        Me.lbl_school_manager_subject_title.Name = "lbl_school_manager_subject_title"
        Me.lbl_school_manager_subject_title.Size = New System.Drawing.Size(50, 13)
        Me.lbl_school_manager_subject_title.TabIndex = 9
        Me.lbl_school_manager_subject_title.Text = "Subject"
        '
        'lbl_school_manager_schoolday_description
        '
        Me.lbl_school_manager_schoolday_description.AutoSize = True
        Me.lbl_school_manager_schoolday_description.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_school_manager_schoolday_description.Location = New System.Drawing.Point(6, 20)
        Me.lbl_school_manager_schoolday_description.Name = "lbl_school_manager_schoolday_description"
        Me.lbl_school_manager_schoolday_description.Size = New System.Drawing.Size(246, 13)
        Me.lbl_school_manager_schoolday_description.TabIndex = 7
        Me.lbl_school_manager_schoolday_description.Text = "Select the day on which you normally have school."
        '
        'dgv_termGrid
        '
        Me.dgv_termGrid.AllowUserToAddRows = False
        Me.dgv_termGrid.AllowUserToDeleteRows = False
        Me.dgv_termGrid.AllowUserToResizeColumns = False
        Me.dgv_termGrid.AllowUserToResizeRows = False
        Me.dgv_termGrid.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgv_termGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgv_termGrid.BackgroundColor = System.Drawing.Color.White
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_termGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.dgv_termGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv_termGrid.ColumnHeadersVisible = False
        Me.dgv_termGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgc_term_ID, Me.dgc_term_term, Me.dgc_term_startdate, Me.dgc_term_enddate})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv_termGrid.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgv_termGrid.Location = New System.Drawing.Point(9, 315)
        Me.dgv_termGrid.MultiSelect = False
        Me.dgv_termGrid.Name = "dgv_termGrid"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv_termGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgv_termGrid.RowHeadersWidth = 25
        Me.dgv_termGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgv_termGrid.Size = New System.Drawing.Size(369, 107)
        Me.dgv_termGrid.TabIndex = 4
        '
        'dgc_term_ID
        '
        Me.dgc_term_ID.HeaderText = "ID"
        Me.dgc_term_ID.Name = "dgc_term_ID"
        Me.dgc_term_ID.Visible = False
        '
        'dgc_term_term
        '
        Me.dgc_term_term.HeaderText = "Term"
        Me.dgc_term_term.Name = "dgc_term_term"
        '
        'dgc_term_startdate
        '
        Me.dgc_term_startdate.HeaderText = "Start Date"
        Me.dgc_term_startdate.Name = "dgc_term_startdate"
        Me.dgc_term_startdate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgc_term_startdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'dgc_term_enddate
        '
        Me.dgc_term_enddate.HeaderText = "End Date"
        Me.dgc_term_enddate.Name = "dgc_term_enddate"
        Me.dgc_term_enddate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgc_term_enddate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'lbl_school_manager_schoolday_title
        '
        Me.lbl_school_manager_schoolday_title.AutoSize = True
        Me.lbl_school_manager_schoolday_title.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_school_manager_schoolday_title.Location = New System.Drawing.Point(6, 4)
        Me.lbl_school_manager_schoolday_title.Name = "lbl_school_manager_schoolday_title"
        Me.lbl_school_manager_schoolday_title.Size = New System.Drawing.Size(66, 13)
        Me.lbl_school_manager_schoolday_title.TabIndex = 7
        Me.lbl_school_manager_schoolday_title.Text = "Schoolday"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "School"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.Width = 204
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Term"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 114
        '
        'CalendarColumn1
        '
        Me.CalendarColumn1.HeaderText = "Start Date"
        Me.CalendarColumn1.Name = "CalendarColumn1"
        Me.CalendarColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CalendarColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CalendarColumn1.Width = 114
        '
        'CalendarColumn2
        '
        Me.CalendarColumn2.HeaderText = "End Date"
        Me.CalendarColumn2.Name = "CalendarColumn2"
        Me.CalendarColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.CalendarColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.CalendarColumn2.Width = 114
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Subject"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Width = 342
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'btn_SchoolGrid_delete
        '
        Me.btn_SchoolGrid_delete.Location = New System.Drawing.Point(48, 498)
        Me.btn_SchoolGrid_delete.Name = "btn_SchoolGrid_delete"
        Me.btn_SchoolGrid_delete.Size = New System.Drawing.Size(30, 23)
        Me.btn_SchoolGrid_delete.TabIndex = 15
        Me.btn_SchoolGrid_delete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btn_SchoolGrid_delete.UseVisualStyleBackColor = True
        '
        'btn_SchoolGrid_add
        '
        Me.btn_SchoolGrid_add.Location = New System.Drawing.Point(12, 498)
        Me.btn_SchoolGrid_add.Name = "btn_SchoolGrid_add"
        Me.btn_SchoolGrid_add.Size = New System.Drawing.Size(30, 23)
        Me.btn_SchoolGrid_add.TabIndex = 16
        Me.btn_SchoolGrid_add.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btn_SchoolGrid_add.UseVisualStyleBackColor = True
        '
        'School_Manager
        '
        Me.AcceptButton = Me.btn_ok
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(675, 533)
        Me.Controls.Add(Me.btn_SchoolGrid_delete)
        Me.Controls.Add(Me.btn_SchoolGrid_add)
        Me.Controls.Add(Me.pnl_Settings_Title)
        Me.Controls.Add(Me.pnl_Settings_Controls)
        Me.Controls.Add(Me.btn_ok)
        Me.Controls.Add(Me.dgv_SchoolGrid)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "School_Manager"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "School Manager"
        CType(Me.dgv_SchoolGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_SubjectGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv_SchooldayGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_Settings_Title.ResumeLayout(False)
        Me.pnl_Settings_Title.PerformLayout()
        Me.pnl_Settings_Controls.ResumeLayout(False)
        Me.pnl_Settings_Controls.PerformLayout()
        CType(Me.dgv_termGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgv_SchoolGrid As System.Windows.Forms.DataGridView
    Friend WithEvents btn_ok As System.Windows.Forms.Button
    Friend WithEvents dgv_SubjectGrid As System.Windows.Forms.DataGridView
    Friend WithEvents dgv_SchooldayGrid As System.Windows.Forms.DataGridView
    Friend WithEvents dgc_school_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_school_school As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_subject_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_subject_subject As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnl_Settings_Title As System.Windows.Forms.Panel
    Friend WithEvents lbl_Settings_Title As System.Windows.Forms.Label
    Friend WithEvents pnl_Settings_Controls As System.Windows.Forms.Panel
    Friend WithEvents lbl_school_manager_schoolday_description As System.Windows.Forms.Label
    Friend WithEvents lbl_school_manager_schoolday_title As System.Windows.Forms.Label
    Friend WithEvents lbl_school_manager_subject_description As System.Windows.Forms.Label
    Friend WithEvents lbl_school_manager_subject_title As System.Windows.Forms.Label
    Friend WithEvents lbl_school_manager_term_description As System.Windows.Forms.Label
    Friend WithEvents lbl_school_manager_term_title As System.Windows.Forms.Label
    Friend WithEvents dgv_termGrid As System.Windows.Forms.DataGridView
    Friend WithEvents dgc_term_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_term_term As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_term_startdate As CalendarColumn
    Friend WithEvents dgc_term_enddate As CalendarColumn
    Friend WithEvents btn_SchooldayGrid_add As System.Windows.Forms.Button
    Friend WithEvents btn_SchooldayGrid_delete As System.Windows.Forms.Button
    Friend WithEvents btn_TermGrid_delete As System.Windows.Forms.Button
    Friend WithEvents btn_TermGrid_add As System.Windows.Forms.Button
    Friend WithEvents btn_SubjectGrid_delete As System.Windows.Forms.Button
    Friend WithEvents btn_SubjectGrid_add As System.Windows.Forms.Button
    Friend WithEvents dgc_Schoolday_ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgc_Schoolday As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CalendarColumn1 As CalendarColumn
    Friend WithEvents CalendarColumn2 As CalendarColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btn_SchoolGrid_delete As System.Windows.Forms.Button
    Friend WithEvents btn_SchoolGrid_add As System.Windows.Forms.Button
End Class

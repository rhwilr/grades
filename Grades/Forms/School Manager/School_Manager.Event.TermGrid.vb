﻿Imports Grades.DB.Model
Imports Grades.DB.Repository

Partial Public Class School_Manager
    Private Sub Event_TermGrid_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) _
        Handles dgv_termGrid.CellEndEdit

        If _
            dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(0).Value = Nothing And
            Not dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then

            Dim Term As New Term

            Term.Identifier = dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(1).Value
            Term.Date_Start = dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(2).Value
            Term.Date_End = dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(3).Value
            Term.FK_School = dgv_SchoolGrid.Rows(dgv_SchoolGrid.CurrentCell.RowIndex).Cells(0).Value

            Connection.GradesContext.Term.AddObject(Term)

            Connection.GradesContext.SaveChanges()


            dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(0).Value = Term.ID

        ElseIf Not dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(1).Value = Nothing Then

            Dim RecordID As Decimal = CDec(dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(0).Value)

            Dim Term = (From _Term In Connection.GradesContext.Term Where _Term.ID = RecordID).Single()

            Term.Identifier = dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(1).Value
            Term.Date_Start = dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(2).Value
            Term.Date_End = dgv_termGrid.Rows(dgv_termGrid.CurrentCell.RowIndex).Cells(3).Value

            Connection.GradesContext.SaveChanges()
        End If
    End Sub
End Class

﻿Imports Grades.Resources

Partial Public Class School_Manager
    Private Sub Initialize_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Localize_School_Manager()
        Initialize_Schoolday_LoadDays()
        Data_SchoolGrid_School()


        btn_SchoolGrid_add.Image = Functions.Resources.ResizeImage(RibbonIcons.Add, 15)
        btn_SchoolGrid_delete.Image = Functions.Resources.ResizeImage(RibbonIcons.Delete, 15)


        btn_SchooldayGrid_add.Image = Functions.Resources.ResizeImage(RibbonIcons.Add, 15)
        btn_SchooldayGrid_delete.Image = Functions.Resources.ResizeImage(RibbonIcons.Delete, 15)

        btn_SubjectGrid_add.Image = Functions.Resources.ResizeImage(RibbonIcons.Add, 15)
        btn_SubjectGrid_delete.Image = Functions.Resources.ResizeImage(RibbonIcons.Delete, 15)

        btn_TermGrid_add.Image = Functions.Resources.ResizeImage(RibbonIcons.Add, 15)
        btn_TermGrid_delete.Image = Functions.Resources.ResizeImage(RibbonIcons.Delete, 15)
    End Sub
End Class

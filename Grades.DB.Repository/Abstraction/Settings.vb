﻿

Public Class Settings
    Public Shared Function GetValue(Key As String)

        Dim _Setting = (From setting In Connection.GradesContext.Settings Where setting.Key = Key).SingleOrDefault

        Return _Setting
    End Function


    Public Shared Sub SetValue(Key As String, Value As Object)

        Dim _Setting As Model.Settings

        _Setting = (From setting In Connection.GradesContext.Settings Where setting.Key = Key).SingleOrDefault

        If IsNothing(_Setting) Then
            _Setting = New Model.Settings
            _Setting.Key = Key

            Select Case True
                Case TypeOf Value Is Integer
                    _Setting.Value_Integer = Value
                Case TypeOf Value Is String
                    _Setting.Value_String = Value
                Case TypeOf Value Is Boolean
                    _Setting.Value_Boolean = Value
            End Select

            Connection.GradesContext.Settings.AddObject(_Setting)
        Else
            _Setting.Key = Key

            Select Case True
                Case TypeOf Value Is Integer
                    _Setting.Value_Integer = Value
                Case TypeOf Value Is String
                    _Setting.Value_String = Value
                Case TypeOf Value Is Boolean
                    _Setting.Value_Boolean = Value
            End Select

        End If

        Connection.GradesContext.SaveChanges()
    End Sub
End Class
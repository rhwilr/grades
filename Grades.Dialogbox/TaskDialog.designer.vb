Partial Class TaskDialog
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing AndAlso (components IsNot Nothing) Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TaskDialog))
        Me.picSystemIcon = New System.Windows.Forms.PictureBox()
        Me.lMsg = New System.Windows.Forms.Label()
        Me.btnNo = New CommandLink()
        Me.btnYes = New CommandLink()
        CType(Me.picSystemIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picSystemIcon
        '
        Me.picSystemIcon.Location = New System.Drawing.Point(21, 19)
        Me.picSystemIcon.Name = "picSystemIcon"
        Me.picSystemIcon.Size = New System.Drawing.Size(36, 36)
        Me.picSystemIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picSystemIcon.TabIndex = 0
        Me.picSystemIcon.TabStop = False
        '
        'lMsg
        '
        Me.lMsg.AutoSize = True
        Me.lMsg.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMsg.Location = New System.Drawing.Point(76, 12)
        Me.lMsg.Name = "lMsg"
        Me.lMsg.Size = New System.Drawing.Size(86, 25)
        Me.lMsg.TabIndex = 0
        Me.lMsg.Text = "Message"
        '
        'btnNo
        '
        Me.btnNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNo.DescriptionText = "No Message"
        Me.btnNo.DialogResult = System.Windows.Forms.DialogResult.No
        Me.btnNo.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNo.HeaderText = "No"
        Me.btnNo.Image = CType(resources.GetObject("btnNo.Image"), System.Drawing.Bitmap)
        Me.btnNo.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.btnNo.ImageVerticalAlign = CommandLink.VerticalAlign.Top
        Me.btnNo.Location = New System.Drawing.Point(32, 138)
        Me.btnNo.Name = "btnNo"
        Me.btnNo.Size = New System.Drawing.Size(244, 50)
        Me.btnNo.TabIndex = 1
        '
        'btnYes
        '
        Me.btnYes.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnYes.DescriptionText = "Yes Message"
        Me.btnYes.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.btnYes.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnYes.HeaderText = "Yes"
        Me.btnYes.Image = CType(resources.GetObject("btnYes.Image"), System.Drawing.Bitmap)
        Me.btnYes.ImageScalingSize = New System.Drawing.Size(32, 32)
        Me.btnYes.ImageVerticalAlign = CommandLink.VerticalAlign.Top
        Me.btnYes.Location = New System.Drawing.Point(32, 82)
        Me.btnYes.Name = "btnYes"
        Me.btnYes.Size = New System.Drawing.Size(244, 50)
        Me.btnYes.TabIndex = 2
        '
        'TaskDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(308, 210)
        Me.Controls.Add(Me.btnNo)
        Me.Controls.Add(Me.btnYes)
        Me.Controls.Add(Me.lMsg)
        Me.Controls.Add(Me.picSystemIcon)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "TaskDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TopMost = True
        CType(Me.picSystemIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private picSystemIcon As System.Windows.Forms.PictureBox
    Private lMsg As System.Windows.Forms.Label
    Private btnYes As CommandLink
    Private btnNo As CommandLink
End Class

Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Text
Imports System.Drawing.Imaging

'Already built-in
<DefaultEvent("Click")>
Public Partial Class CommandLink
    Inherits UserControl

    Public Sub New()
        InitializeComponent()
        'Smooth redrawing
        Me.DoubleBuffered = True
    End Sub

#Region "Fields---------------------------------"

    Private Enum State
        Normal
        Hover
        Pushed
        Disabled
    End Enum

    Public Enum VerticalAlign
        Top
        Middle
        Bottom
    End Enum

    Private FormState As State = State.Normal
    Private offset As Integer = 0

    Private m_headerText As String = "Header Text"
    Private m_descriptionText As String = "Description"

    Private m_image As Bitmap
    Private grayImage As Bitmap
    Private imageSize As New Size(24, 24)
    Private imageAlign As VerticalAlign = VerticalAlign.Top

    Private descriptFont As Font
    Private diagResult As DialogResult = DialogResult.None

#End Region

#Region "Properties----------------------------"

    <Category("Command Appearance"), Browsable(True), DefaultValue("Header Text")>
    Public Property HeaderText() As String
        Get
            Return m_headerText
        End Get
        Set
            m_headerText = value
            Me.Refresh()
        End Set
    End Property

    <Category("Command Appearance"), Browsable(True), DefaultValue("Description")>
    Public Property DescriptionText() As String
        Private Get
            Return m_descriptionText
        End Get
        Set
            m_descriptionText = value
            Me.Refresh()
        End Set
    End Property

    <Category("Command Appearance"), Browsable(True), DefaultValue(CStr(Nothing))>
    Public Property Image() As Bitmap
        Get
            Return m_image
        End Get
        Set(value As Bitmap)
            'Clean up
            If m_image IsNot Nothing Then
                m_image.Dispose()
            End If

            If grayImage IsNot Nothing Then
                grayImage.Dispose()
            End If

            m_image = value
            If m_image IsNot Nothing Then
                grayImage = GetGrayscale(m_image)
            Else
                'generate image for disabled state
                grayImage = Nothing
            End If
            Me.Refresh()
        End Set
    End Property

    <Category("Command Appearance"), Browsable(True), DefaultValue(GetType(Size), "24,24")>
    Public Property ImageScalingSize() As Size
        Get
            Return imageSize
        End Get
        Set
            imageSize = value
            Me.Refresh()
        End Set
    End Property

    <Category("Command Appearance"), Browsable(True), DefaultValue(VerticalAlign.Top)>
    Public Property ImageVerticalAlign() As VerticalAlign
        Get
            Return imageAlign
        End Get
        Set
            imageAlign = value
            Me.Refresh()
        End Set
    End Property

    <Category("Command Appearance")>
    Public Overrides Property Font() As Font
        Get
            Return MyBase.Font
        End Get
        Set
            MyBase.Font = value

            'Clean up
            If descriptFont IsNot Nothing Then
                descriptFont.Dispose()
            End If

            'Update the description font, which is the same just 3 sizes smaller
            descriptFont = New Font(Me.Font.FontFamily, Me.Font.Size - 3)
        End Set
    End Property

    <Category("Behavior"), DefaultValue(DialogResult.None)>
    Public Property DialogResult() As DialogResult
        Get
            Return diagResult
        End Get
        Set
            diagResult = value
        End Set
    End Property

#End Region

#Region "Events-----------------------------------"

    Protected Overrides Sub OnPaint(e As PaintEventArgs)
        MyBase.OnPaint(e)
        'draws the regular background stuff
        If Me.Focused AndAlso FormState = State.Normal Then
            DrawHighlight(e.Graphics)
        End If

        Select Case FormState
            Case State.Normal
                DrawNormalState(e.Graphics)
                Exit Select
            Case State.Hover
                DrawHoverState(e.Graphics)
                Exit Select
            Case State.Pushed
                DrawPushedState(e.Graphics)
                Exit Select
            Case State.Disabled
                DrawNormalState(e.Graphics)
                'DrawForeground takes care of drawing the disabled state
                Exit Select
            Case Else
                Exit Select
        End Select
    End Sub

    Protected Overrides Sub OnClick(e As EventArgs)
        MyBase.OnClick(e)
        If diagResult <> DialogResult.None Then
            Me.ParentForm.DialogResult = diagResult
            Me.ParentForm.Close()
        End If
    End Sub

    Protected Overrides Sub OnKeyPress(e As KeyPressEventArgs)
        If e.KeyChar = Convert.ToChar(Keys.Enter) Then
            Me.PerformClick()
        End If

        MyBase.OnKeyPress(e)
    End Sub

    Protected Overrides Sub OnGotFocus(e As EventArgs)
        Me.Refresh()
        MyBase.OnGotFocus(e)
    End Sub

    Protected Overrides Sub OnLostFocus(e As EventArgs)
        Me.Refresh()
        MyBase.OnLostFocus(e)
    End Sub

    Protected Overrides Sub OnMouseEnter(e As EventArgs)
        If Me.Enabled Then
            FormState = State.Hover
        End If
        Me.Refresh()

        MyBase.OnMouseEnter(e)
    End Sub

    Protected Overrides Sub OnMouseLeave(e As EventArgs)
        If Me.Enabled Then
            FormState = State.Normal
        End If
        Me.Refresh()

        MyBase.OnMouseLeave(e)
    End Sub

    Protected Overrides Sub OnMouseDown(e As MouseEventArgs)
        If Me.Enabled Then
            FormState = State.Pushed
        End If
        Me.Refresh()

        MyBase.OnMouseDown(e)
    End Sub

    Protected Overrides Sub OnMouseUp(e As MouseEventArgs)
        If Me.Enabled Then
            If Me.RectangleToScreen(Me.ClientRectangle).Contains(Cursor.Position) Then
                FormState = State.Hover
            Else
                FormState = State.Normal
            End If
        End If
        Me.Refresh()

        MyBase.OnMouseUp(e)
    End Sub

    Protected Overrides Sub OnEnabledChanged(e As EventArgs)
        If Me.Enabled Then
            FormState = State.Normal
        Else
            FormState = State.Disabled
        End If

        Me.Refresh()

        MyBase.OnEnabledChanged(e)
    End Sub

    Protected Overrides Sub Dispose(disposing As Boolean)
        If disposing Then
            m_image.Dispose()
            grayImage.Dispose()
            descriptFont.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

#End Region

#Region "Drawing Methods-------------------------"

    'Draws the light-blue rectangle around the button when it is focused (by Tab for example)
    Private Sub DrawHighlight(g As Graphics)
        'The outline is drawn inside the button
        Dim innerRegion As GraphicsPath = RoundedRect(Me.Width - 3, Me.Height - 3, 3)

        '----Shift the inner region inwards
        Dim translate As New Matrix()
        translate.Translate(1, 1)
        innerRegion.Transform(translate)
        translate.Dispose()
        '-----

        Dim inlinePen As New Pen(Color.FromArgb(192, 233, 243))
        'Light-blue
        g.SmoothingMode = SmoothingMode.AntiAlias

        g.DrawPath(inlinePen, innerRegion)

        'Clean-up
        inlinePen.Dispose()
        innerRegion.Dispose()
    End Sub

    'Draws the button when the mouse is over it
    Private Sub DrawHoverState(g As Graphics)
        Dim outerRegion As GraphicsPath = RoundedRect(Me.Width - 1, Me.Height - 1, 3)
        Dim innerRegion As GraphicsPath = RoundedRect(Me.Width - 3, Me.Height - 3, 2)
        '----Shift the inner region inwards
        Dim translate As New Matrix()
        translate.Translate(1, 1)
        innerRegion.Transform(translate)
        translate.Dispose()
        '-----
        Dim backgroundRect As New Rectangle(1, 1, Me.Width - 2, CInt(Math.Truncate(Me.Height*0.75F)) - 2)

        Dim outlinePen As New Pen(Color.FromArgb(189, 189, 189))
        'SystemColors.ControlDark
        Dim inlinePen As New Pen(Color.FromArgb(245, 255, 255, 255))
        'Slightly transparent white
        'Gradient brush for the background, goes from white to transparent 75% of the way down
        Dim _
            backBrush As _
                New LinearGradientBrush(New Point(0, 0), New Point(0, backgroundRect.Height), Color.White,
                                        Color.Transparent)
        backBrush.WrapMode = WrapMode.TileFlipX
        'keeps the gradient smooth incase of the glitch where there's an extra gradient line
        g.SmoothingMode = SmoothingMode.AntiAlias

        g.FillRectangle(backBrush, backgroundRect)
        g.DrawPath(inlinePen, innerRegion)
        g.DrawPath(outlinePen, outerRegion)

        'Text/Image
        offset = 0
        'Text/Image doesn't move
        DrawForeground(g)

        'Clean up
        outlinePen.Dispose()
        inlinePen.Dispose()
        outerRegion.Dispose()
        innerRegion.Dispose()
    End Sub

    'Draws the button when it's clicked down
    Private Sub DrawPushedState(g As Graphics)
        Dim outerRegion As GraphicsPath = RoundedRect(Me.Width - 1, Me.Height - 1, 3)
        Dim innerRegion As GraphicsPath = RoundedRect(Me.Width - 3, Me.Height - 3, 2)
        '----Shift the inner region inwards
        Dim translate As New Matrix()
        translate.Translate(1, 1)
        innerRegion.Transform(translate)
        translate.Dispose()
        '-----
        Dim backgroundRect As New Rectangle(1, 1, Me.Width - 3, Me.Height - 3)

        Dim outlinePen As New Pen(Color.FromArgb(167, 167, 167))
        'Outline is darker than normal
        Dim inlinePen As New Pen(Color.FromArgb(227, 227, 227))
        'Darker white
        Dim backBrush As New SolidBrush(Color.FromArgb(234, 234, 234))
        'SystemColors.ControlLight
        g.SmoothingMode = SmoothingMode.AntiAlias

        g.FillRectangle(backBrush, backgroundRect)
        g.DrawPath(inlinePen, innerRegion)
        g.DrawPath(outlinePen, outerRegion)

        'Text/Image
        offset = 1
        'moves image inwards 1 pixel (x and y) to create the illusion that the button was pushed
        DrawForeground(g)

        'Clean up
        outlinePen.Dispose()
        inlinePen.Dispose()
        outerRegion.Dispose()
        innerRegion.Dispose()
    End Sub

    'Draws the button in it's regular state
    Private Sub DrawNormalState(g As Graphics)
        'Nothing needs to be drawn but the text and image

        'Text/Image
        offset = 0
        'Text/Image doesn't move
        DrawForeground(g)
    End Sub

    'Draws Text and Image
    Private Sub DrawForeground(g As Graphics)
        'Make sure drawing is of good quality
        g.TextRenderingHint = TextRenderingHint.ClearTypeGridFit
        g.PixelOffsetMode = PixelOffsetMode.HighQuality

        'Image Coordinates-------------------------------
        Dim imageLeft As Integer = 9
        Dim imageTop As Integer = 0
        '

        'Text Layout--------------------------------
        'Gets the width/height of the text once it's drawn out
        Dim headerLayout As SizeF = g.MeasureString(m_headerText, Me.Font)
        Dim descriptLayout As SizeF = g.MeasureString(m_descriptionText, descriptFont)

        'Merge the two sizes into one big rectangle
        Dim _
            totalRect As _
                New Rectangle(0, 0, CInt(Math.Truncate(Math.Max(headerLayout.Width, descriptLayout.Width))),
                              CInt(Math.Truncate(headerLayout.Height + descriptLayout.Height)) - 4)

        'Align the total rectangle-------------------------
        If m_image IsNot Nothing Then
            totalRect.X = imageLeft + imageSize.Width + 5
        Else
            'consider the image is there
            totalRect.X = 20
        End If

        totalRect.Y = (Me.Height\2) - (totalRect.Height\2)
        'center vertically  
        '---------------------------------------------------
        'Align the top of the image---------------------
        If m_image IsNot Nothing Then
            Select Case imageAlign
                Case VerticalAlign.Top
                    imageTop = totalRect.Y
                    Exit Select
                Case VerticalAlign.Middle
                    imageTop = totalRect.Y + (totalRect.Height\2) - (imageSize.Height\2)
                    Exit Select
                Case VerticalAlign.Bottom
                    imageTop = totalRect.Y + totalRect.Height - imageSize.Height
                    Exit Select
                Case Else
                    Exit Select
            End Select
        End If
        '-----------------------------------------------

        'Brushes--------------------------------
        ' Determine text color depending on whether the control is enabled or not
        Dim textColor As Color = Me.ForeColor
        If Not Me.Enabled Then
            textColor = SystemColors.GrayText
        End If

        Dim textBrush As New SolidBrush(textColor)
        '------------------------------------------

        g.DrawString(m_headerText, Me.Font, textBrush, totalRect.Left + offset, totalRect.Top + offset)
        g.DrawString(DescriptionText, descriptFont, textBrush, totalRect.Left + 1 + offset,
                     totalRect.Bottom - CInt(Math.Truncate(descriptLayout.Height)) + offset)
        'Note: the + 1 in "totalRect.Left + 1 + offset" compensates for GDI+ inconsistency

        If m_image IsNot Nothing Then
            If Me.Enabled Then
                g.DrawImage(m_image,
                            New Rectangle(imageLeft + offset, imageTop + offset, imageSize.Width, imageSize.Height))
            Else
                'make sure there is a gray-image
                If grayImage Is Nothing Then
                    grayImage = GetGrayscale(m_image)
                End If
                'generate grayscale now
                g.DrawImage(grayImage,
                            New Rectangle(imageLeft + offset, imageTop + offset, imageSize.Width, imageSize.Height))
            End If
        End If

        'Clean-up
        textBrush.Dispose()
    End Sub

#End Region

#Region "Helper Methods--------------------------"

    Private Shared Function RoundedRect(width As Integer, height As Integer, radius As Integer) As GraphicsPath
        Dim baseRect As New RectangleF(0, 0, width, height)
        Dim diameter As Single = radius*2F
        Dim sizeF As New SizeF(diameter, diameter)
        Dim arc As New RectangleF(baseRect.Location, sizeF)
        Dim path As New GraphicsPath()

        ' top left arc 
        path.AddArc(arc, 180, 90)

        ' top right arc 
        arc.X = baseRect.Right - diameter
        path.AddArc(arc, 270, 90)

        ' bottom right arc 
        arc.Y = baseRect.Bottom - diameter
        path.AddArc(arc, 0, 90)

        ' bottom left arc
        arc.X = baseRect.Left
        path.AddArc(arc, 90, 90)

        path.CloseFigure()
        Return path
    End Function

    Private Shared Function GetGrayscale(original As Image) As Bitmap
        'Set up the drawing surface
        Dim grayscale As New Bitmap(original.Width, original.Height)
        Dim g As Graphics = Graphics.FromImage(grayscale)

        'Grayscale Color Matrix
        Dim _
            colorMatrix As _
                New ColorMatrix(
                    New Single()() _
                                   {New Single() {0.3F, 0.3F, 0.3F, 0, 0}, New Single() {0.59F, 0.59F, 0.59F, 0, 0},
                                    New Single() {0.11F, 0.11F, 0.11F, 0, 0}, New Single() {0, 0, 0, 1, 0},
                                    New Single() {0, 0, 0, 0, 1}})

        'Create attributes
        Dim att As New ImageAttributes()
        att.SetColorMatrix(colorMatrix)

        'Draw the image with the new attributes
        g.DrawImage(original, New Rectangle(0, 0, original.Width, original.Height), 0, 0, original.Width,
                    original.Height,
                    GraphicsUnit.Pixel, att)

        'Clean up
        g.Dispose()

        Return grayscale
    End Function

    Private Sub PerformClick()
        Me.OnClick(Nothing)
    End Sub

#End Region
End Class

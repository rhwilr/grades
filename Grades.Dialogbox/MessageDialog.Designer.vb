﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MessageDialog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lbl_Title = New System.Windows.Forms.Label()
        Me.picSystemIcon = New System.Windows.Forms.PictureBox()
        Me.pnl_bottom = New System.Windows.Forms.Panel()
        Me.btnL = New System.Windows.Forms.Button()
        Me.btnR = New System.Windows.Forms.Button()
        Me.btnC = New System.Windows.Forms.Button()
        Me.lbl_Message = New System.Windows.Forms.Label()
        CType(Me.picSystemIcon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl_bottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_Title
        '
        Me.lbl_Title.AutoSize = True
        Me.lbl_Title.Font = New System.Drawing.Font("Segoe UI", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_Title.Location = New System.Drawing.Point(76, 13)
        Me.lbl_Title.Name = "lbl_Title"
        Me.lbl_Title.Size = New System.Drawing.Size(48, 25)
        Me.lbl_Title.TabIndex = 2
        Me.lbl_Title.Text = "Title"
        '
        'picSystemIcon
        '
        Me.picSystemIcon.Location = New System.Drawing.Point(21, 20)
        Me.picSystemIcon.Name = "picSystemIcon"
        Me.picSystemIcon.Size = New System.Drawing.Size(36, 36)
        Me.picSystemIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picSystemIcon.TabIndex = 1
        Me.picSystemIcon.TabStop = False
        '
        'pnl_bottom
        '
        Me.pnl_bottom.BackColor = System.Drawing.SystemColors.Control
        Me.pnl_bottom.Controls.Add(Me.btnC)
        Me.pnl_bottom.Controls.Add(Me.btnL)
        Me.pnl_bottom.Controls.Add(Me.btnR)
        Me.pnl_bottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnl_bottom.Location = New System.Drawing.Point(0, 76)
        Me.pnl_bottom.Name = "pnl_bottom"
        Me.pnl_bottom.Size = New System.Drawing.Size(353, 43)
        Me.pnl_bottom.TabIndex = 3
        '
        'btnL
        '
        Me.btnL.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnL.Location = New System.Drawing.Point(185, 8)
        Me.btnL.Name = "btnL"
        Me.btnL.Size = New System.Drawing.Size(75, 23)
        Me.btnL.TabIndex = 0
        Me.btnL.Text = "Yes"
        Me.btnL.UseVisualStyleBackColor = True
        Me.btnL.Visible = False
        '
        'btnR
        '
        Me.btnR.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnR.Location = New System.Drawing.Point(266, 8)
        Me.btnR.Name = "btnR"
        Me.btnR.Size = New System.Drawing.Size(75, 23)
        Me.btnR.TabIndex = 0
        Me.btnR.Text = "No"
        Me.btnR.UseVisualStyleBackColor = True
        Me.btnR.Visible = False
        '
        'btnC
        '
        Me.btnC.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnC.Location = New System.Drawing.Point(21, 8)
        Me.btnC.Name = "btnC"
        Me.btnC.Size = New System.Drawing.Size(75, 23)
        Me.btnC.TabIndex = 0
        Me.btnC.Text = "OK"
        Me.btnC.UseVisualStyleBackColor = True
        Me.btnC.Visible = False
        '
        'lbl_Message
        '
        Me.lbl_Message.AutoSize = True
        Me.lbl_Message.Location = New System.Drawing.Point(78, 43)
        Me.lbl_Message.Name = "lbl_Message"
        Me.lbl_Message.Size = New System.Drawing.Size(50, 13)
        Me.lbl_Message.TabIndex = 4
        Me.lbl_Message.Text = "Message"
        Me.lbl_Message.Visible = False
        '
        'MessageDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(353, 119)
        Me.Controls.Add(Me.lbl_Message)
        Me.Controls.Add(Me.pnl_bottom)
        Me.Controls.Add(Me.lbl_Title)
        Me.Controls.Add(Me.picSystemIcon)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MessageDialog"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.TopMost = True
        CType(Me.picSystemIcon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl_bottom.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents lbl_Title As System.Windows.Forms.Label
    Private WithEvents picSystemIcon As System.Windows.Forms.PictureBox
    Friend WithEvents pnl_bottom As System.Windows.Forms.Panel
    Friend WithEvents btnR As System.Windows.Forms.Button
    Friend WithEvents btnL As System.Windows.Forms.Button
    Friend WithEvents btnC As System.Windows.Forms.Button
    Private WithEvents lbl_Message As System.Windows.Forms.Label
End Class

﻿Imports System.Windows.Forms
Imports System.Drawing
Imports Grades.Localization


Public Class MessageDialog
    Inherits Form

    Public Enum IconType
        Application
        [Error]
        Information
        Question
        Shield
        Warning
    End Enum

    Public Enum ButtonType
        OK
        OKCancel
        RetryCancel
        YesNo
    End Enum

#Region "CONSTRUCTORS ----------------------------------------"

    Public Sub New(Title As String, Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        AdjustSize()
    End Sub

    Public Sub New(Title As String, Message As String, Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetMessage(Message)
        AdjustSize()
    End Sub

    Public Sub New(Title As String, Message As String, Caption As String, Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetMessage(Message)
        SetCaption(Caption)
        AdjustSize()
    End Sub


    Public Sub New(Title As String, Icon As IconType, Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetIcon(Icon)
        AdjustSize()
    End Sub

    Public Sub New(Title As String, Message As String, Icon As IconType, Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetMessage(Message)
        SetIcon(Icon)
        AdjustSize()
    End Sub

    Public Sub New(Title As String, Message As String, Caption As String, Icon As IconType,
                   Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetMessage(Message)
        SetCaption(Caption)
        SetIcon(Icon)
        AdjustSize()
    End Sub


    Public Sub New(Title As String, Button As ButtonType, Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetButtons(Button)
        AdjustSize()
    End Sub

    Public Sub New(Title As String, Message As String, Button As ButtonType,
                   Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetMessage(Message)
        SetButtons(Button)
        AdjustSize()
    End Sub

    Public Sub New(Title As String, Message As String, Caption As String, Button As ButtonType,
                   Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetMessage(Message)
        SetCaption(Caption)
        SetButtons(Button)
        AdjustSize()
    End Sub


    Public Sub New(Title As String, Icon As IconType, Button As ButtonType, Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetIcon(Icon)
        SetButtons(Button)
        AdjustSize()
    End Sub

    Public Sub New(Title As String, Message As String, Icon As IconType, Button As ButtonType,
                   Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetMessage(Message)
        SetIcon(Icon)
        SetButtons(Button)
        AdjustSize()
    End Sub

    Public Sub New(Title As String, Message As String, Caption As String, Icon As IconType, Button As ButtonType,
                   Optional CompressedString As Boolean = False)
        InitializeComponent()
        SetTitle(Title, CompressedString)
        SetMessage(Message)
        SetCaption(Caption)
        SetIcon(Icon)
        SetButtons(Button)
        AdjustSize()
    End Sub

#End Region


#Region "METHODS ------------------------------------------"

    Private Sub AdjustSize()

        
        'Location Message
        lbl_Message.Top = lbl_Title.Bottom + 5


        'Width Title
        If lbl_Title.Left + lbl_Title.Width + 32 >= Me.Width Then
            'Label goes outside the Form, resize the Form
            Me.Width += (lbl_Title.Left + lbl_Title.Width + 32) - Me.Width
        End If

        ' Width Message
        If lbl_Message.Left + lbl_Message.Width + 32 >= Me.Width Then
            'Label goes outside the Form, resize the Form
            Me.Width += (lbl_Message.Left + lbl_Message.Width + 32) - Me.Width
        End If

        'Height
        If lbl_Message.Top + lbl_Message.Height + 25 >= pnl_bottom.Top Then
            'Label goes on top of the Yes button, resize the Form
            Me.Height += (lbl_Message.Top + lbl_Message.Height + 25) - pnl_bottom.Top
        End If
    End Sub


    Private Function TextLineBreak(input As String) As String
        Return input.Replace("\n", vbCrLf)
    End Function

    Private Sub SetTitle(Title As String, Optional CompressedString As Boolean = False)

        SetButtons(ButtonType.OK)

        If CompressedString Then

            Dim Split As String()

            Split = Title.Split(";")

            If Split.Length = 0 Then
                Split(0) = ""
                Split(1) = ""
            ElseIf Split.Length = 1 Then
                ReDim Preserve Split(2)
                Split(1) = ""
            End If

            lbl_Title.Text = TextLineBreak(Split(0))
            SetMessage(Split(1))
            SetCaption(Split(0))
        Else
            lbl_Title.Text = TextLineBreak(Title)
        End If
    End Sub

    Private Sub SetMessage(Message As String)
        lbl_Message.Visible = True
        lbl_Message.Text = TextLineBreak(Message)
    End Sub

    Private Sub SetCaption(Caption As String)
        Me.Text = Caption
    End Sub

    Private Sub SetIcon(Icon As IconType)
        Dim iconImg As Image = Nothing

        Select Case Icon
            Case IconType.Application
                iconImg = DirectCast(SystemIcons.Application.ToBitmap(), Image)
                Exit Select
            Case IconType.[Error]
                iconImg = DirectCast(SystemIcons.[Error].ToBitmap(), Image)
                Exit Select
            Case IconType.Information
                iconImg = DirectCast(SystemIcons.Information.ToBitmap(), Image)
                Exit Select
            Case IconType.Question
                iconImg = DirectCast(SystemIcons.Question.ToBitmap(), Image)
                Exit Select
            Case IconType.Shield
                iconImg = DirectCast(SystemIcons.Shield.ToBitmap(), Image)
                Exit Select
            Case IconType.Warning
                iconImg = DirectCast(SystemIcons.Warning.ToBitmap(), Image)
                Exit Select
            Case Else
                Exit Select
        End Select


        picSystemIcon.Image = iconImg
    End Sub

    Private Sub SetButtons(Button As ButtonType)

        Select Case Button
            Case ButtonType.OK

                btnC.Visible = True
                btnC.DialogResult = DialogResult.OK
                btnC.TabIndex = 1
                btnC.Text = GeneralUI.OK

                Dim w As Integer = Me.Width/2
                Dim btnw As Integer = Me.btnC.Width/2
                Dim ttw As Integer = w - btnw
                btnC.Location = New Point(ttw, btnC.Location.Y)

                btnL.Visible = False
                btnR.Visible = False
                Exit Select

            Case ButtonType.OKCancel

                btnL.Visible = True
                btnR.Visible = True

                btnL.DialogResult = DialogResult.OK
                btnR.DialogResult = DialogResult.Cancel

                btnL.TabIndex = 1
                btnR.TabIndex = 2

                btnL.Text = GeneralUI.OK
                btnR.Text = GeneralUI.Cancel

                btnC.Visible = False

                Exit Select
            Case ButtonType.RetryCancel

                btnL.Visible = True
                btnR.Visible = True

                btnL.DialogResult = DialogResult.Retry
                btnR.DialogResult = DialogResult.Cancel

                btnL.TabIndex = 1
                btnR.TabIndex = 2

                btnL.Text = GeneralUI.Retry
                btnR.Text = GeneralUI.Cancel

                btnC.Visible = False

                Exit Select
            Case ButtonType.YesNo

                btnL.Visible = True
                btnR.Visible = True

                btnL.DialogResult = DialogResult.Yes
                btnR.DialogResult = DialogResult.No

                btnL.TabIndex = 1
                btnR.TabIndex = 2

                btnL.Text = GeneralUI.Yes
                btnR.Text = GeneralUI.No

                btnC.Visible = False

                Exit Select
            Case Else
                Exit Select
        End Select
    End Sub

#End Region
End Class
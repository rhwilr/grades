'----------------------------------------------------
' Downloaded From
' Visual C# Kicks - http://www.vcskicks.com/
'----------------------------------------------------
Imports System.Windows.Forms
Imports System.Drawing

Partial Public Class TaskDialog
    Inherits Form

    Public Enum IconType
        Application
        [Error]
        Information
        Question
        Shield
        Warning
    End Enum

#Region "CONSTRUCTORS ----------------------------------------"

    Public Sub New(Message As String)
        InitializeComponent()

        lMsg.Text = Message
        AdjustSize()
    End Sub

    Public Sub New(Message As String, Icon As IconType)
        Me.New(Message)
        'call base constructor
        Dim iconImg As Image = Nothing

        Select Case Icon
            Case IconType.Application
                iconImg = DirectCast(SystemIcons.Application.ToBitmap(), Image)
                Exit Select
            Case IconType.[Error]
                iconImg = DirectCast(SystemIcons.[Error].ToBitmap(), Image)
                Exit Select
            Case IconType.Information
                iconImg = DirectCast(SystemIcons.Information.ToBitmap(), Image)
                Exit Select
            Case IconType.Question
                iconImg = DirectCast(SystemIcons.Question.ToBitmap(), Image)
                Exit Select
            Case IconType.Shield
                iconImg = DirectCast(SystemIcons.Shield.ToBitmap(), Image)
                Exit Select
            Case IconType.Warning
                iconImg = DirectCast(SystemIcons.Warning.ToBitmap(), Image)
                Exit Select
            Case Else
                Exit Select
        End Select


        picSystemIcon.Image = iconImg
    End Sub

    Public Sub New(Message As String, Caption As String, Icon As IconType)
        Me.New(Message, Icon)
        Me.Text = Caption
    End Sub

#End Region

#Region "METHODS ------------------------------------------"

    Private Sub AdjustSize()
        'Width
        If lMsg.Left + lMsg.Width + 32 >= Me.Width Then
            'Label goes outside the Form, resize the Form
            Me.Width += (lMsg.Left + lMsg.Width + 32) - Me.Width
        End If

        'Height
        If lMsg.Top + lMsg.Height + 25 >= btnYes.Top Then
            'Label goes on top of the Yes button, resize the Form
            Me.Height += (lMsg.Top + lMsg.Height + 25) - btnYes.Top
        End If
    End Sub

#End Region

#Region "PROPERTY METHODS --------------------------------------------"

    Public Sub SetButtonYesText(Header As String, Description As String)
        btnYes.HeaderText = Header
        btnYes.DescriptionText = Description
    End Sub

    Public Sub SetButtonNoText(Header As String, Description As String)
        btnNo.HeaderText = Header
        btnNo.DescriptionText = Description
    End Sub

#End Region
End Class
